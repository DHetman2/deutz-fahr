<?php

function sendConfirmationMail( $recipient_name, $recipient_mail ) {

  $subject = 'Deutz-Fahr Ciągniki Seria 6, 7 i 8 - potwierdzenie wypełnienia formularza';
  $message = '
  <html>
  <head>
    <title>eutz-Fahr Ciągniki Seria 6, 7 i 8 - potwierdzenie wypełnienia formularza</title>
  </head>
  <body>
    <img src="https://promocje-deutz-fahr.pl/ciagniki-6-7-8/img/deutz-fahr-logo.png" src="Deutz Fahr">
    <p style="font-weight:bold">Dziękujemy za zaufanie do marki DEUTZ-FAHR i wypełnienie formularza.<p>
    <p>Już wkrótce skontaktujemy się z Tobą w celu przedstawienia indywidualnej oferty na <strong>ciągniki DEUTZ-FAHR</strong>.</p>
    <p>Do usłyszenia,<br>
    Zespół DEUTZ-FAHR</p>
  </body>
  </html>';

  require_once('class.phpmailer.php');    //dodanie klasy phpmailer
  require_once('class.smtp.php');    //dodanie klasy smtp
  $mail = new PHPMailer(true);    //utworzenie nowej klasy phpmailer
  $mail->isSMTP();
  $mail->CharSet = 'UTF-8';
  $mail->Host = 'localhost';
  $mail->SMTPAuth = true;
  $mail->Username = 'auto@promocje-deutz-fahr.pl';
  $mail->Password = 'HbxO3Dw1#dZ';
  $mail->setFrom('auto@promocje-deutz-fahr.pl','Deutz-Fahr');
  $mail->addAddress($recipient_mail);
  $mail->Subject = $subject;
  $mail->Body    = $message;
  $mail->IsHTML(true);
  $mail->send();

}

function sendConfirmationMailToDF( $recipient_name, $recipient_mail, $recipient_phone, $seria, $powiat, $zgoda_dane, $zgoda_handlowa1, $zgoda_handlowa2 ) {

  $zgoda_dane = ( $zgoda_dane == 1 ) ? 'Tak' : 'Nie';
  $zgoda_handlowa1 = ( $zgoda_handlowa1 == 1 ) ? 'Tak' : 'Nie';
  $zgoda_handlowa2 = ( $zgoda_handlowa2 == 1 ) ? 'Tak' : 'Nie';

  $subject = 'Nowy lead - Deutz-Fahr Ciągniki Seria 6, 7 i 8 (2022)';
  $message = '<html>
  <head>
  <title>Nowy lead - eutz-Fahr Ciągniki Seria 6, 7 i 8 (2022)</title>
  </head>
  <body>
  <p style="font-weight:bold">Nowy lead - Ciągniki Deutz-Fahr Serie 6, 7 i 8</p>
  <p>Nowa osoba wysłała swoje zgłoszenie poprzez formularz Ciągniki Deutz-Fahr 6, 7 i 8:<p>
  <p><strong>Seria: ' . $seria . '</strong><br>
  Imię i nazwisko: ' . $recipient_name . '<br>
  E-mail: ' . $recipient_mail . '<br>
  Telefon: ' . $recipient_phone . '<br>
  Powiat: ' . $powiat . '<br>
  Zgoda na przetwarzanie danych: ' . $zgoda_dane . '<br>
  Zgoda na kontakt: ' . $zgoda_handlowa1 . '<br>
  Zgoda na inne wykorzystanie: ' . $zgoda_handlowa2 . '<br>
  </p>
  <p>Miłego dnia,<br>
  Zespół Deutz-Fahr</p>
  </body>
  </html>';

  require_once('class.phpmailer.php');    //dodanie klasy phpmailer
    require_once('class.smtp.php');    //dodanie klasy smtp
    $mail = new PHPMailer(true);    //utworzenie nowej klasy phpmailer
  $mail->isSMTP();
  $mail->CharSet = 'UTF-8';
  $mail->Host = 'localhost';
  $mail->SMTPAuth = true;
  $mail->Username = 'auto@promocje-deutz-fahr.pl';
  $mail->Password = 'HbxO3Dw1#dZ';
  $mail->setFrom('auto@promocje-deutz-fahr.pl','Deutz-Fahr');
  $mail->addAddress('jaroslaw.figurski@sdfgroup.pl');
  $mail->addAddress('rafal.jarmul@sdfgroup.pl');
  // $mail->addAddress('damian.hetman@adagri.com'); // Testowy adres e-mail
  $mail->Subject = $subject;
  $mail->Body    = $message;
  $mail->IsHTML(true);
  $mail->send();


}

?>