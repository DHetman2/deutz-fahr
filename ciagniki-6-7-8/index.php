<?php

$base = 'https://promocje-deutz-fahr.pl/ciagniki-6-7-8';
// $base = 'http://localhost.lo/Deutz-Fahr/ciagniki-6-7-8';
$version = 220713;

// Włącz plik form.php ze skryptami formularza
require( 'form.php' );

// Zabezpiecz przekierowaniem przed podwójną wysyłką formularza
redirect();

?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <title>Ciągniki Deutz-Fahr - miara Twojego sukcesu</title>
    <meta charset="utf-8">
    <meta name="description" content="Stworzyliśmy efektywne i efektowne ciągniki Deutz-Fahr w taki sposób, żeby pomogły ci w realizacji Twojej indywidualnej strategii, by stały się Twoją wizytówką i rozwijały Twoje gospodarstwo.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?= $base; ?>/css/grid.css?=<?= $version; ?>">
    <link rel="stylesheet" type="text/css" href="<?= $base; ?>/css/style.css?=<?= $version; ?>">
    <link rel="stylesheet" href="<?= $base; ?>/css/glightbox.min.css?=<?= $version; ?>">
    <link rel="stylesheet" href="<?= $base; ?>/css/splide.min.css?=<?= $version; ?>">
    <link rel="icon" href="<?= $base; ?>/img/favicon.png" type="image/png">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700;800&display=swap" rel="stylesheet">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WB5VB9K');</script>
    <!-- End Google Tag Manager -->

</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WB5VB9K"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Header – logo + menu -->

<header>
    <div class="row">
        <div class="l4 xs10 col" id="logo">
            <?php require( 'img/deutz-fahr-logo.svg' ); ?>
        </div>
        <div class="l8 xs2 text-right col">
            <div class="mobile-menu-button l-hide m-hide s-hide xs-show" onclick="ga('send', 'event', 'Hamburger menu', 'click', 'Hamburger Menu' );">
                <?php require( 'img/hamburger-menu.svg'); ?>
            </div>
            <div class="mobile-menu">
                <a href="#form" class="header-cta">
                    <?php require( 'img/przymierz-ciagnik.svg' ); ?>Zapytaj o ciągnik
                </a>
                <ul class="s-hide xs-show">
                    <li><a href="#seria-6" onclick="ga('send', 'event', 'Seria', 'click', 'Seria 6');">Seria 6</a></li>
                    <li><a href="#seria-6-ttv" onclick="ga('send', 'event', 'Seria', 'click', 'Seria 6 TTV');">Seria 6 TTV</a></li>
                    <li><a href="#seria-7-ttv" onclick="ga('send', 'event', 'Seria', 'click', 'Seria 7 TTV');">Seria 7 TTV</a></li>
                    <li><a href="#seria-8-ttv" onclick="ga('send', 'event', 'Seria', 'click', 'Seria 8 TTV');">Seria 8 TTV</a></li>
                </ul>
                <a href="#" class="mobile-menu-button-close l-hide m-hide s-hide xs-show">
                    Schowaj menu
                </a>
            </div>
        </div>
    </div>
</header>

<section class="hero">
    <div class="row">
        <picture>
            <source media="(max-width: 480px)" srcset="<?= $base; ?>/img/ciagniki-deutz-fahr-480-b.jpg 1x, <?= $base; ?>/img/ciagniki-deutz-fahr-480-b@2x.jpg 2x">
            <source media="(max-width: 970px)" srcset="<?= $base; ?>/img/ciagniki-deutz-fahr-970-b.jpg 1x, <?= $base; ?>/img/ciagniki-deutz-fahr-970-b@2x.jpg 2x">
            <img src="<?= $base; ?>/img/ciagniki-deutz-fahr-b.jpg" srcset="<?= $base; ?>/img/ciagniki-deutz-fahr-b.jpg 1x, <?= $base; ?>/img/ciagniki-deutz-fahr-b@2x.jpg 2x" alt="Ciągniki Deutz-Fahr - miara Twojego sukcesu">
        </picture>
    </div>
</section>

<?php

thankYouPage();

if ( !isset( $_GET['thank-you'] ) ) :

?>

    <!-- Nagłowek oraz tekst główny -->
    <section class="intro">
        <div class="row">
            <div class="l12 col">
                <h1>Mierz wysoko <strong class="green">z ciągnikami <nobr>Deutz-Fahr</nobr></strong></h1>
            </div>
            <div class="l6 s12 col">
                <p>Dzięki Twoim decyzjom i ciężkiej pracy rodzi się sukces. Maszyny, których używasz, są zarówno częścią, jak i&nbsp;miarą Twojego sukcesu.</p>
                <p>To właśnie ciągniki pokazują, jakim rolnikiem jesteś i&nbsp;co jest dla Ciebie ważne. To one sprawiają, że inni biorą z&nbsp;Ciebie przykład. Stworzyliśmy więc <strong class="green">efektywne i&nbsp;efektowne ciągniki Deutz-Fahr</strong> w&nbsp;taki sposób, żeby pomogły ci w&nbsp;realizacji Twojej indywidualnej strategii, by stały się Twoją wizytówką i&nbsp;rozwijały Twoje gospodarstwo.</p>
                <a href="#form" class="button">Zapytaj o ciągnik <span><?php require( 'img/arrow.svg' ); ?></span></a>
            </div>
            <div class="l6 s12 col">
                <img data-src="<?= $base; ?>/img/deutz-fahr-6.png" data-srcset="<?= $base; ?>/img/deutz-fahr-6@2x.png 2x" alt="Ciągnik Deutz-Fahr Seria 6">
            </div>
        </div>
    </section>

    <!-- Formularz -->

    <section class="formularz">

        <div class="row">

            <div class="l6 s12 col">

                <img data-src="<?= $base; ?>/img/deutz-fahr-6.png" data-srcset="<?= $base; ?>/img/deutz-fahr-6@2x.png 2x" alt="Ciągnik Deutz-Fahr Seria 6">

            </div>

            <div class="l6 s12 col">

                <?php

                $validation = false;
                $error = [];

                saveData();

                if ( $validation == false || count( $error ) > 0 ) {

                    form(1);

                }

                ?>

            </div>

        </div>

    </section>

    <!-- Seria 6 -->
    <section class="seria white" id="seria-6">
    
        <div class="row">

            <div class="l6 s12 col">

                <h2>Seria 6</h2>
                <p class="lead">Optymalne połączenie najnowszych rozwiązań</p>
                <hr>
                <p>Ciągniki DEUTZ-FAHR Serii 6 to optymalne połączenie najnowszych rozwiązań na miarę Twoich potrzeb. Dajemy ci możliwość wybrania najlepszych kombinacji rozwiązań i wyposażenia własnego, niepowtarzalnego ciągnika. Seria 6 to 10 modeli <strong>o mocy od 156 do 226&nbsp;KM</strong>, trzy rozstawy osi, trzy wersje napędu, różne rodzaje kabin i&nbsp;<strong>szereg dodatkowych opcji wyposażenia</strong>, które możesz dobrać do własnych indywidualnych potrzeb.</p>

                <a href="#form" class="button seria-6">Zapytaj o ciągnik <span><?php require( 'img/arrow.svg' ); ?></span></a>

            </div>

            <div class="l6 s12 col">

                <div class="splide splide-6" role="group">
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/jd6-1.jpg" alt="Ciągnik Deutz-Fahr Seria 6">
                            </li>
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/jd6-2.jpg" alt="Ciągnik Deutz-Fahr Seria 6">
                            </li>
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/jd6-3.jpg" salt="Ciągnik Deutz-Fahr Seria 6">
                            </li>
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/jd6-4.jpg" salt="Ciągnik Deutz-Fahr Seria 6">
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <!-- Seria 6 TTV -->
    <section class="seria" id="seria-6-ttv">
    
        <div class="row">

            <div class="l12 col text-center mini">
                <span>Potrzebujesz więcej?</span>
            </div>

            <div class="l6 s12 col">

                <h2>Seria 6 TTV</h2>
                <p class="lead">Najlepsi gracze na każdym polu</p>
                <hr>
                <p>Niezawodna i komfortowa seria ciągników Deutz Fahr dla mierzących wyżej. Maszyny z&nbsp;tej serii wyposażone są w&nbsp;sprawdzone silniki Deutz TCD 6.1 Stage V. Ich <strong>moc maksymalna to nawet 230&nbsp;KM</strong>. Traktory serii 6&nbsp;TTV posiadają <strong>najnowszą przekładnię TTV</strong>, która zapewnia płynność jazdy oraz znacznie poprawia wydajność. Po raz pierwszy ciągniki z&nbsp;tej serii o&nbsp;mocy powyżej 200&nbsp;KM można prowadzić z&nbsp;<strong>prędkością nawet 60&nbsp;km/h</strong>.</p>

                <a href="#form" class="button seria-6-ttv">Zapytaj o ciągnik <span><?php require( 'img/arrow.svg' ); ?></span></a>

            </div>

            <div class="l6 s12 col">

                <div class="splide splide-6ttv" role="group">
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/jd6ttv-1.jpg" alt="Ciągnik Deutz-Fahr Seria 6">
                            </li>
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/jd6ttv-2.jpg" alt="Ciągnik Deutz-Fahr Seria 6">
                            </li>
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/jd6ttv-3.jpg" salt="Ciągnik Deutz-Fahr Seria 6">
                            </li>
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/jd6ttv-4.jpg" salt="Ciągnik Deutz-Fahr Seria 6">
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <!-- Seria 7 TTV -->
    <section class="seria white" id="seria-7-ttv">
    
        <div class="row">

            <div class="l12 col text-center mini">
                <span>Dokładamy jeszcze więcej mocy!</span>
            </div>

            <div class="l6 s12 col">

                <h2>Seria 7 TTV</h2>
                <p class="lead">Lider w swojej klasie</p>
                <hr>
                <p>Silne, wydajne i niezawodne. Takie są ciągniki DEUTZ-FAHR Serii 7&nbsp;TTV. To liderzy w&nbsp;swojej klasie. Na miarę Twojego sukcesu. Pod maską kryją silnik Deutz TCD 6.1 Stage&nbsp;V <strong>o&nbsp;maksymalnej mocy 247 KM</strong>. Dzięki swoim parametrom <strong>ciągniki z&nbsp;Serii 7&nbsp;TTV są liderami w&nbsp;swojej klasie</strong>. A&nbsp;specjalna limitowana edycja modelu ciągnika o&nbsp;nazwie Warrior przeznaczona do najtrudniejszych prac na polu i&nbsp;na drodze zrobi ogromne wrażenie i&nbsp;pozwoli wyróżnić się z&nbsp;tłumu!</p>

                <a href="#form" class="button seria-7-ttv">Zapytaj o ciągnik <span><?php require( 'img/arrow.svg' ); ?></span></a>

            </div>

            <div class="l6 s12 col">

                <div class="splide splide-7ttv" role="group">
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/jd7ttv-1.jpg" alt="Ciągnik Deutz-Fahr Seria 7 TTV">
                            </li>
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/jd7ttv-2.jpg" alt="Ciągnik Deutz-Fahr Seria 7 TTV">
                            </li>
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/jd7ttv-3.jpg" salt="Ciągnik Deutz-Fahr Seria 7 TTV">
                            </li>
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/jd7ttv-4.jpg" salt="Ciągnik Deutz-Fahr Seria 7 TTV">
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <!-- Seria 8 TTV -->
    <section class="seria" id="seria-8-ttv">
    
        <div class="row">

            <div class="l12 col text-center mini">
                <span>Potrzebne ci turbodoładowanie?</span>
            </div>

            <div class="l6 s12 col">

                <h2>Seria 8 TTV</h2>
                <p class="lead">Gotowy do działania</p>
                <hr>
                <p>Ciągniki dla mierzących najwyżej! Najnowsze rozwiązania technologiczne sprawiają, że&nbsp;ciągniki DEUTZ-FAHR Serii 8&nbsp;TTV, w&nbsp;pełni wykorzystują potencjał podwójnie turbodoładowanego silnika, który generuje <strong>moc 287 KM</strong>. To maszyny wydajne i efektywne, które pozwalają oszczędzić koszty i&nbsp;czas. Mogą pochwalić się skrzynią o&nbsp;prędkości 40&nbsp;km/h SuperECO, przednim WOM o&nbsp;dwóch prędkościach oraz <strong>zwinnością na polu i&nbsp;w&nbsp;transporcie</strong>.</p>

                <a href="#form" class="button seria-8-ttv">Zapytaj o ciągnik <span><?php require( 'img/arrow.svg' ); ?></span></a>

            </div>

            <div class="l6 s12 col">

                <div class="splide splide-8ttv" role="group">
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/jd8ttv-1.jpg" alt="Ciągnik Deutz-Fahr Seria 8 TTV">
                            </li>
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/jd8ttv-2.jpg" alt="Ciągnik Deutz-Fahr Seria 8 TTV">
                            </li>
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/jd8ttv-3.jpg" salt="Ciągnik Deutz-Fahr Seria 8 TTV">
                            </li>
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/jd8ttv-4.jpg" salt="Ciągnik Deutz-Fahr Seria 8 TTV">
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <section class="row">

        <div class="l12 col relative">

            <div class="warrior">

                <div class="l6 s12 col">

                    <h2>Limitowana wersja <nobr>Deutz-Fahr</nobr> Warrior</h2>
                    <p class="lead">Dla indywidualistów</p>
                    <hr>
                    <p>Nasz Warrior robi wrażenie. Imponujący design, jeszcze większy komfort w kabinie, specjalny zestaw oświetleniowy Warrior LED, wykończenia ze stali nierdzewnej, układ automatycznie sterowanej klimatyzacji oraz wiele więcej. Sprawdź limitowane modele w Seriach 6, 6 TTV, 7 TTV i 8 TTV.</p>

                    <a href="#form" class="button">Zapytaj o ciągnik <span><?php require( 'img/arrow.svg' ); ?></span></a>

                </div>

                <div class="l6 s12 col">

                    <img data-src="<?= $base; ?>/img/deutz-fahr-warrior.png" data-srcset="<?= $base; ?>/img/deutz-fahr-warrior@2x.png 2x" alt="Ciągnik Deutz-Fahr Warrior">

                </div>

            </div>

        </div>

    </section>

    <section class="boxy white">

        <div class="row">

            <div class="l12 col">
                <h3>Najlepsze i niezawodne rozwiązania dla ciebie.<span>Made in germany.</span></h3>
            </div>

            <div class="frames">

                <div class="col">
                    <div>
                        <h4>Gwarancja precyzji i wydajności</h4>
                        <p>W najnowocześniejszym w Europie zakładzie produkcji ciągników, uruchomionym w Lauingen w Niemczech, projektujemy precyzyjne i wydajne ciągniki, które są odpowiedzią na wyzwania nowoczesnego rolnictwa. To maszyny zdolne do bezkompromisowej pracy w najtrudniejszych i najbardziej zróżnicowanych warunkach.</p>
                    </div>
                </div>

                <div class="col">
                    <div>
                        <h4>Niezawodna i sprawdzona technologia</h4>
                        <p>Tworzone przez nas modele ciągników rolniczych mają być bezdyskusyjnymi liderami w swojej klasie. Zapewnia to najnowocześniejsza, sprawdzona i niezawodna technologia – najwyższej jakości podzespoły, innowacyjne rozwiązania mechatroniczne i najnowsza przekładnia TTV. Dzięki nim traktory Deutz-Fahr mogą pochwalić się niezwykłą efektywnością.</p>
                    </div>
                </div>

            </div>

            <div class="l12 col text-center">
                <a href="#form" class="button">Zapytaj o ciągnik <span><?php require( 'img/arrow.svg' ); ?></span></a>
            </div>

        </div>

    </section>

    <!-- Galeria zdjęć -->
    <section id="gallery">

        <div class="row section-margin">
            <div class="l12 col text-center">
                <h3>Ciągniki deutz-fahr w akcji</h3>
            </div>
        </div>

        <div class="row">

            <div class="l3 m4 s6 col">
                <a href="<?= $base; ?>/img/jd1.jpg" class="glightbox2" onclick="ga('send', 'event', 'Zdjęcie galerii', 'click', 'jd1');">
                    <img data-src="<?= $base; ?>/img/jd1-thumb.jpg">
                </a>
            </div>
            <div class="l3 m4 s6 col">
                <a href="<?= $base; ?>/img/jd2.jpg" class="glightbox2" onclick="ga('send', 'event', 'Zdjęcie galerii', 'click', 'jd2');">
                    <img data-src="<?= $base; ?>/img/jd2-thumb.jpg">
                </a>
            </div>
            <div class="l3 m4 s6 col">
                <a href="<?= $base; ?>/img/jd3.jpg" class="glightbox2" onclick="ga('send', 'event', 'Zdjęcie galerii', 'click', 'jd3');">
                    <img data-src="<?= $base; ?>/img/jd3-thumb.jpg">
                </a>
            </div>
            <div class="l3 m4 s6 col">
                <a href="<?= $base; ?>/img/jd4.jpg" class="glightbox2" onclick="ga('send', 'event', 'Zdjęcie galerii', 'click', 'jd4');">
                    <img data-src="<?= $base; ?>/img/jd4-thumb.jpg">
                </a>
            </div>

            <div class="l3 m4 s6 col">
                <a href="<?= $base; ?>/img/jd5.jpg" class="glightbox2" onclick="ga('send', 'event', 'Zdjęcie galerii', 'click', 'jd5');">
                    <img data-src="<?= $base; ?>/img/jd5-thumb.jpg">
                </a>
            </div>
            <div class="l3 m4 s6 col">
                <a href="<?= $base; ?>/img/jd6.jpg" class="glightbox2" onclick="ga('send', 'event', 'Zdjęcie galerii', 'click', 'jd6');">
                    <img data-src="<?= $base; ?>/img/jd6-thumb.jpg">
                </a>
            </div>
            <div class="l3 m4 s6 col">
                <a href="<?= $base; ?>/img/jd7.jpg" class="glightbox2" onclick="ga('send', 'event', 'Zdjęcie galerii', 'click', 'jd7');">
                    <img data-src="<?= $base; ?>/img/jd7-thumb.jpg">
                </a>
            </div>
            <div class="l3 m4 s6 col">
                <a href="<?= $base; ?>/img/jd8.jpg" class="glightbox2" onclick="ga('send', 'event', 'Zdjęcie galerii', 'click', 'jd8');">
                    <img data-src="<?= $base; ?>/img/jd8-thumb.jpg">
                </a>
            </div>

        </div>

        <div class="row">
            <div class="l12 col text-center">
                <a href="#form" class="button">Zapytaj o ciągnik <span><?php require( 'img/arrow.svg' ); ?></span></a>
            </div>
        </div>

    </section>

    <?php

endif;

?>

<a href="#form" class="mobile-cta" onclick="ga('send', 'event', 'Przyklejone menu mobilne', 'click', 'Przyklejone menu mobilne');">
    <div class="row">
        <div class="l6 col">
            <p>Ciągniki dostępne<br>od teraz</p>
        </div>
        <div class="l6 col">
            <?php require( 'img/arrow.svg' ); ?><br>
            Zapytaj o ciągnik
        </div>
    </div>
</a>

<footer>
    <div class="row">
        <div class="l12 col text-right">
            <a href="#logo" class="up">Wróć do góry</a>
            <ul>
                <li><a class="instagram" href="https://www.instagram.com/deutzfahr_brand_official/" target="_blank">Instagram</a></li>
                <li><a class="facebook" href="https://www.facebook.com/DeutzFahrPolska/" target="_blank">Facebook</a></li>
                <li><a class="youtube" href="https://www.youtube.com/user/sdfpoland/videos?disable_polymer=1" target="_blank">YouTube</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="l6 xs12 col">
            <p>DEUTZ&#8209;FAHR JEST MARKĄ <img data-src="<?= $base; ?>/img/samedeutz-fahr.png" alt="SDF"></p>
        </div>
        <div class="l6 xs12 col">
            <a href="https://www.deutz-fahr.com/pl-pl/uwagi-prawne-i-ochrona-prywatnosci" target="_blank">Polityka prywatności</a>
        </div>
    </div>
</footer>

<script src="<?= $base; ?>/js/cookie.js?=<?= $version; ?>"></script>
<script src="<?= $base; ?>/js/glightbox.min.js?=<?= $version; ?>"></script>
<script src="<?= $base; ?>/js/general.js?=<?= $version; ?>"></script>
<script src="<?= $base; ?>/js/form.js?=<?= $version; ?>"></script>
<script src="<?= $base; ?>/js/splide.min.js?=<?= $version; ?>"></script>

</body>
</html>
