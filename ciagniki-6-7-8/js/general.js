function openFAQ () {

    var tabs = document.querySelectorAll('.question');
    for ( var i = 0; i<tabs.length; i++ ) {
        tabs[i].addEventListener('click', function() {
            if ( this.getAttribute( 'data-open' ) == 'true' )  {
                this.setAttribute( 'data-open', 'false' );
            } else {
                this.setAttribute( 'data-open', 'true' );
            }
        })
    }

}

function openButtons() {

    var questions = document.querySelectorAll('[data-target]');

    for ( var i = 0; i<questions.length; i++ ) {

        questions[i].addEventListener( 'click', function(event) {
            event.preventDefault();

            var target = this.getAttribute( 'data-target' );
            var targetElement = document.querySelector( '[data-name="' + target + '"]' );

            if ( targetElement.getAttribute( 'data-show' ) == 'true' ) {
                targetElement.setAttribute( 'data-show', 'false' );
                this.setAttribute( 'data-show', 'false' );
            } else {
                targetElement.setAttribute( 'data-show', 'true' );
                this.setAttribute( 'data-show', 'true' );
            }

        });

    }

}

function openInfographic() {


    // Otwieranie okienek
    var infographic = document.querySelector( '.infografika' );
    var point = document.querySelectorAll( '.point, .points-mobile div' );

    for ( var i = 0; i<point.length; i++ ) {

        point[i].addEventListener( 'click', function() {

            // Wyłączanie tekstów
            var text = infographic.querySelectorAll( '.explanation .text' );
            for (var i = 0; i<text.length; i++ ) {
                text[i].setAttribute( 'data-show', 'false' );
            }

            var target = this.getAttribute( 'data-target' );
            var targetElement = document.querySelector( '[data-name="' + target + '"]' );

            targetElement.setAttribute( 'data-show', 'true' );
            this.setAttribute( 'data-show', 'true' );

            infographic.querySelector( '.explanation' ).setAttribute( 'data-show', 'true' );

        });

    }

    // Zamykanie
    if ( infographic ) {
        infographic.querySelector( '.close' ).addEventListener( 'click', function() {

            infographic.querySelector( '.explanation' ).setAttribute( 'data-show', 'false' );

            // Wyłączanie tekstów
            var text = infographic.querySelectorAll( '.explanation .text' );
            for (var i = 0; i<text.length; i++ ) {
                text[i].setAttribute( 'data-show', 'false' );
            }

        });
    }


}

function CTAscrollAndFocusForm() {

    var cta = document.querySelectorAll( '[href="#formularz"]' );
    for ( var i = 0; i<cta.length; i++ ) {

        cta[i].addEventListener( 'click', function( event ) {
            event.preventDefault();
            document.querySelector( '#formularz' ).scrollIntoView();
            document.querySelector( 'input[name="name"]' ).focus();
        });

    }

}

function mobileMenu() {

    if ( document.querySelector( '.mobile-menu-button') ) {

        document.querySelector( '.mobile-menu-button').addEventListener( 'click', function() {

            document.querySelector( '.mobile-menu' ).setAttribute( 'data-show', 'true');

        });

        document.querySelector( '.mobile-menu-button-close').addEventListener( 'click', function() {

            document.querySelector( '.mobile-menu' ).setAttribute( 'data-show', 'false' );

        });

        // Wyłącznie menu po kliknięciu jakiegokolwiek linka w menu
        var links = document.querySelectorAll( '.mobile-menu a');

        for (var i = 0; i<links.length; i++ ) {

            links[i].addEventListener( 'click', function() {
                document.querySelector( '.mobile-menu' ).setAttribute( 'data-show', 'false' );
            });

        }


    }

}

window.addEventListener('load', function() {

    openFAQ();
    openButtons();
    openInfographic();
    CTAscrollAndFocusForm();
    mobileMenu();

    // Lightbox do galerii zdjęć
    GLightbox({
        selector: '.glightbox2'
    });

    // Slidery zdjęć
    if ( document.querySelector( '.splide-6' ) ) {

        var splide6 = new Splide( '.splide-6', {
            pagination: false
        } );
        splide6.mount({});

        var splide6ttv = new Splide( '.splide-6ttv', {
            pagination: false
        } );
        splide6ttv.mount({});

        var splide7ttv = new Splide( '.splide-7ttv', {
            pagination: false
        } );
        splide7ttv.mount({});

        var splide8ttv = new Splide( '.splide-8ttv', {
            pagination: false
        } );
        splide8ttv.mount({});

    }

    // Lazy loading
    var img = document.querySelectorAll( 'img[data-src]' );
    for ( var i = 0;  i<img.length; i++ ) {

        if ( img[i].getAttribute( 'data-srcset' ) ) {
            var srcset = img[i].getAttribute( 'data-srcset' );
            if ( srcset ) {
                img[i].setAttribute( 'srcset', srcset );
            }   
        }

        var src = img[i].getAttribute( 'data-src' );
        img[i].setAttribute( 'src', src );

    }

});