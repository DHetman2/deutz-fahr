<?php

define( 'LEADS_LOCALHOST', '213.77.69.224' );
define( 'LEADS_USER', 'promocjasdf' );
define( 'LEADS_PASS', '6GBCu9iY6xHc' );
define( 'LEADS_DATABASE', 'promocjasdf' );

//define( 'LEADS_LOCALHOST', 'localhost' );
//define( 'LEADS_DATABASE', 'leads' );
//define( 'LEADS_USER', 'root' );
//define( 'LEADS_PASS', '' );

/**
 * Sometimes you will find that your website will not get the correct user IP after adding CDN, then this function will help you
 * 
 * @return string
 */
function getRealIp()
{
	$ip = $_SERVER['REMOTE_ADDR'];
	
    if( isset( $_SERVER[ 'HTTP_X_FORWARDED_FOR' ] ) && preg_match_all( '#\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}#s', $_SERVER[ 'HTTP_X_FORWARDED_FOR' ], $matches ) )
	{
        foreach( $matches[ 0 ] as $xip )
		{
            if( !preg_match( '#^(10|172\.16|192\.168)\.#', $xip ) )
			{
                $ip = $xip;
                break;
            }
        }
    }
	else if( isset( $_SERVER[ 'HTTP_CLIENT_IP' ] ) && preg_match( '/^([0-9]{1,3}\.){3}[0-9]{1,3}$/', $_SERVER[ 'HTTP_CLIENT_IP' ] ) )
	{
		$ip = $_SERVER[ 'HTTP_CLIENT_IP' ];
    }
	else if( isset( $_SERVER[ 'HTTP_CF_CONNECTING_IP' ] ) && preg_match( '/^([0-9]{1,3}\.){3}[0-9]{1,3}$/', $_SERVER[ 'HTTP_CF_CONNECTING_IP' ] ) )
	{
        $ip = $_SERVER[ 'HTTP_CF_CONNECTING_IP' ];
    }
	else if( isset( $_SERVER[ 'HTTP_X_REAL_IP' ] ) && preg_match( '/^([0-9]{1,3}\.){3}[0-9]{1,3}$/', $_SERVER[ 'HTTP_X_REAL_IP' ] ) )
	{
        $ip = $_SERVER['HTTP_X_REAL_IP'];
    }
	
    return $ip;

}

function isRequestFromPoland()
{
	$ip	   = getRealIp();
	$table = '';

	if( filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) )
	{
		$table = 'ip_v4_pl';
	}
	else if( filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6 ) )
	{
		$table = 'ip_v6_pl';
	}
	else
	{
		return false;
	}

	//lets check access from Poland
	$mysqli   = new mysqli( LEADS_LOCALHOST, LEADS_USER, LEADS_PASS, LEADS_DATABASE );
	$check    = $mysqli->query("SELECT * FROM $table WHERE INET_ATON('$ip') BETWEEN ip_from AND ip_to" );
	$checkrow = $check->num_rows;

	if( !$checkrow )
	{
		return false;
	}
	
	return true;
}