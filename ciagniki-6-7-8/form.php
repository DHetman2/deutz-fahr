<?php
require("leads_config.php");

function redirect() {
    ob_start();
    session_start();
    if ( isset( $_GET['thank-you'] ) && $_SESSION['send'] != true ) {
        $_SESSION[ 'send' ] = false;
        header( 'location: https://promocje-deutz-fahr.pl/ciagniki-6-7-8' );
    }
    $_SESSION[ 'send' ] = false;
}

function thankYouPage() {

    global $base;

    if ( isset( $_GET['thank-you'] ) ) : ?>

        <section class="row thank-you">

            <div class="l12 m12 s12 text-center col">

                <img src="<?= $base; ?>/img/sent-mail.svg" alt="Zgłoszenie wysłane">
                <h2>Dziękujemy za przesłanie formularza</h2>
                <p>Już wkrótce odezwiemy się do Ciebie w sprawie ciągników Deutz-Fahr.</p>

            </div>

        </section>

        </body>
        </html>

    <?php

    endif;

}

function saveData() {

    // ini_set('display_errors', 1);
    // error_reporting(E_ALL);

    if ( $_SERVER['REQUEST_METHOD'] == 'POST') :

        $data = filter_input_array(INPUT_POST, [
            "seria" => FILTER_SANITIZE_STRING,
            "name" => FILTER_SANITIZE_STRING,
            "code" => FILTER_SANITIZE_STRING,
            "phone" => FILTER_SANITIZE_STRING,
            "email" => FILTER_SANITIZE_EMAIL,
            "powiat" => FILTER_SANITIZE_STRING,
        ]);

        $data["zgoda_dane"] = ( isset( $_POST['zgoda_dane'] ) ) ? 1 : 0;
        $data["zgoda_handlowa1"] = ( isset( $_POST['zgoda_handlowa1'] ) ) ? 1 : 0;
        $data["zgoda_handlowa2"] = ( isset( $_POST['zgoda_handlowa2'] ) ) ? 1 : 0;
        $date = date('Y-m-d H:i:s');

        if ( !empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif ( !empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $error = [];
        if ( trim($data['name']) == '' ) { $error["name"] = true; }
        if ( trim($data['phone']) == '' ) { $error["phone"] = true; }
        if ( $data['zgoda_dane'] != 1 ) { $error["zgoda_dane"] = true; }
        if ( $data['zgoda_handlowa1'] != 1 ) { $error["zgoda_handlowa1"] = true; }
		if ( strstr( $data['name'], 'www' ) || strstr( $data['name'], 'http' ) ) { $error["name"] = true; }
		
		// if( !isRequestFromPoland() )
		// {
		// 	$error["bad_IP"] = true;
			
		// 	echo '<center><p><strong>Brak dostępu dla IP spoza Polski. Jeśli mimo wszystko nie jesteś botem, skontaktuj się z nami.</strong></p></center>';
		// }
		
        // Dodaj do bazy
        if ( count( $error ) == 0 ) {

            $mysqli = new mysqli( LEADS_LOCALHOST, LEADS_USER, LEADS_PASS, LEADS_DATABASE );
            mysqli_set_charset($mysqli, "utf8");
            $mysqli->query("SET SESSION sql_mode=''");

            $check = $mysqli->query("SELECT * FROM lp_ciagniki_6_7_8 WHERE phone = '".$data['phone']."' ");
            $checkrow = $check->num_rows;

            // Sprawdź czy taki numer telefonu jest już w bazie (jeśli jest to nie dodawaj ponownie do bazy)
            if ( $checkrow == null ) {

                $result = $mysqli->query("INSERT INTO lp_ciagniki_6_7_8 VALUES ('', '".$data['name']."', '".$data['phone']."', '".$data['email']."', '".$data['seria']."', '".$data['powiat']."', '".$data['zgoda_dane']."', '".$data['zgoda_handlowa1']."', '".$data['zgoda_handlowa2']."', '".$date."', '".$ip."' ) ") or $mysqli->error;

            } else {

                $result = true;

            }

            if ( $result ) {

                $_SESSION['send'] = true;

                // Wyślij maila
                require('mail.php');
                if ( !empty( $data['email'] ) ) {
                    sendConfirmationMail( $data['name'], $data['email'] );
                }

                sendConfirmationMailToDF( $data['name'], $data['email'], $data['phone'], $data['seria'], $data['powiat'], $data['zgoda_dane'], $data['zgoda_handlowa1'], $data['zgoda_handlowa2'] );

                // Przekieruj na stronę podziękowania
                header ('location: /ciagniki-6-7-8/dziekujemy/');

            } else {

                echo '<p>Błąd, skontaktuj się z nami.</p>';

            }

        }

    endif;

}

function form( $i ) { ?>

    <form method="POST" action="" id="form" class="form" data-id="<?= $i; ?>">

        <h2>Zapytaj o&nbsp;szczegóły</h2>
        <p>Zostaw nam dane kontaktowe w&nbsp;poniższym formularzu, a&nbsp;nasi doradcy skontaktują się z&nbsp;Tobą i&nbsp;zaproponują indywidualną ofertę. Odpowiedzą również na Twoje pytania.</p>

        <div class="field seria">
            <label>Seria ciągników*</label>
            <select name="seria" required>
                <option value="" selected="selected" disabled>Wybierz serię ciągnika</option>
                <option value="6">Seria 6</option>
                <option value="6 TTV">Seria 6 TTV</option>
                <option value="7 TTV">Seria 7 TTV</option>
                <option value="8 TTV">Seria 8 TTV</option>
            </select>
            <span <?= ( $error['seria'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span>
        </div>
        <div class="field name no-padding-top">
            <label>Imię <sup>*</sup></label>
            <input type="text" name="name" value="<?= ( isset($_POST['name']) ) ? htmlspecialchars($_POST['name']) : ''; ?>" required>
            <span <?= ( $error['name'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span>
        </div>
        <div class="field phone">
            <label>Numer telefonu<sup>*</sup></label>
            <input type="tel" name="phone" value="<?= ( isset($_POST['phone']) ) ? htmlspecialchars($_POST['phone']) : ''; ?>" placeholder="___ ___ ___" pattern="[0-9]{3} [0-9]{3} [0-9]{3}" maxlength="11" equired>
            <span <?= ( $error['phone'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span>
        </div>
        <div class="field email">
            <label>Adres e-mail<sup>*</sup></label>
            <input type="email" name="email" value="<?= ( isset($_POST['email']) ) ? htmlspecialchars($_POST['email']) : ''; ?>" placeholder="_____@_____.___" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$">
            <span <?= ( $error['email'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span>
        </div>
        <div class="field powiat">
            <label>Powiat*</label>
            <input type="text" name="powiat" value="<?= ( isset($_POST['powiat']) ) ? htmlspecialchars($_POST['powiat']) : ''; ?>" list="powiaty" required>
            <span <?= ( $error['powiat'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span>
            <?php require('powiaty.php'); ?>
        </div>

        <div class="data-admin">

            <p>Informacja o przetwarzaniu danych osobowych przez SAME DEUTZ-FAHR Italia S.p.A. – <span class="more-button" data-target="more">czytaj więcej. <i class="fas fa-angle-down"></i></span></p>

            <div class="more" data-name="more">
                <p>Państwa dane osobowe  będą przetwarzane przez Administratora danych osobowych, tj. SAME DEUTZ‑FAHR Italia S.p.A. z siedzibą przy Viale Francesco Cassani, 14 - 24047 Treviglio (BG) – Włochy na podstawie wyrażonej przez Państwa zgody (art. 6 ust. 1 lit. a RODO) i tylko w celu zarządzania Państwa prośbą dotyczącą kontaktu oraz w celu dostarczenia Państwu żądanych informacji. Podanie danych osobowych jest dobrowolne. Brak zgody uniemożliwi nam jednak realizację Państwa prośby. Dane będą przetwarzane przez Administratora wyłącznie przez czas konieczny do zrealizowania deklarowanego celu, tj. przez czas trwania kampanii marketingowej „NIE PRZEGAP OKAZJI! ŻNIWA Z NOWYM KOMBAJNEM” (o ile przepisy prawa nie stanowią inaczej). Państwa dane osobowe zostaną udostępnione partnerowi handlowemu Administratora (dealerowi marki DEUTZ‑FAHR w Polsce), właściwemu ze względu na treść zgłoszonego przez Państwa żądania kontaktu. Informacja o dealerach dostępna jest pod linkiem: <a href="https://www.deutz-fahr.com/pl-pl/znajdz-dealera">https://www.deutz-fahr.com/pl-pl/znajdz-dealera</a>.</p>
                <p>Państwa dane osobowe nie będą przetwarzane w celu zautomatyzowanego podejmowania decyzji, w tym poprzez profilowanie. 
                W każdym czasie mogą Państwo wycofać zgodę na przetwarzanie Państwa danych osobowych. Wycofanie zgody nie wpływa na zgodność z prawem przetwarzania, którego dokonano na podstawie zgody przed jej wycofaniem.</p>
                <p>Mają Państwo prawo do uzyskania potwierdzenia występowania bądź braku Państwa danych. Ponadto przysługuje Państwu prawo do uzyskania informacji na temat źródła i celu oraz sposobu przetwarzania danych, prawo do ich aktualizacji, sprostowania i uzupełnienia, a także do usunięcia danych przetwarzanych niezgodnie z prawem, co do przetwarzania których zgoda została wycofana lub jeśli dane te nie są już niezbędne do celów, dla których zostały zebrane. W celu skorzystania z przysługujących uprawnień mogą Państwo zwrócić się do administratora danych, kontaktując się z SAME DEUTZ‑FAHR Italia S.p.A. z siedzibą przy Viale Francesco Cassani, 14 - 24047 Treviglio (BG) – Włochy bądź wysyłając wiadomość e-mail na adres: privacy@sdfgroup.com.
                W razie uznania, że Państwa dane osobowe są przetwarzane niezgodnie z prawem, mają Państwo prawo do wystąpienia ze skargą do organu nadzorczego, tj. Prezesa Urzędu Ochrony Danych Osobowych w Polsce albo do organu wiodącego, tj. Garante per la protezione dei dati personali we Włoszech.</p>
            </div>
        </div>

        <div class="field checkbox zgoda_dane">
            <input type="checkbox" id="zgoda_dane<?= $i; ?>" name="zgoda_dane" required <?= ( isset($_POST['zgoda_dane']) == true ) ? 'checked="checked"' : ''; ?>>
            <label for="zgoda_dane<?= $i; ?>">
                Wyrażam zgodę na przetwarzanie przez SAME DEUTZ-FAHR Italia S.p.A. i przez jej partnerów handlowych (dealerów marki DEUTZ-FAHR w Polsce) moich danych osobowych w celu przedstawiania mi informacji o interesujących mnie produktach, w tym informacji marketingowych*.
            </label>
            <span <?= ( $error['zgoda_dane'] == true ) ? 'class="show"' : '' ; ?>>Zaznaczenie tego pola jest obowiązkowe</span>
        </div>

        <div class="field checkbox zgoda_handlowa1">
            <input type="checkbox" id="zgoda_handlowa1_<?= $i; ?>" name="zgoda_handlowa1" required <?= ( isset($_POST['zgoda_handlowa1']) == true ) ? 'checked="checked"' : ''; ?>>
            <label for="zgoda_handlowa1_<?= $i; ?>">
                Wyrażam zgodę na kontakt telefoniczny lub e-mail przez SAME DEUTZ-FAHR Italia S.p.A. lub jej partnerów handlowych (dealerów marki DEUTZ-FAHR w Polsce) w celu przedstawiania mi informacji o interesujących mnie produktach, w tym informacji marketingowych*.
            </label>
            <span <?= ( $error['zgoda_handlowa1'] == true ) ? 'class="show"' : '' ; ?>>Zaznaczenie tego pola jest obowiązkowe</span>
        </div>

        <div class="field checkbox zgoda_handlowa2">
            <input type="checkbox" id="zgoda_handlowa2_<?= $i; ?>" name="zgoda_handlowa2" required <?= ( isset($_POST['zgoda_handlowa2']) == true ) ? 'checked="checked"' : ''; ?>>
            <label for="zgoda_handlowa2_<?= $i; ?>">
                Wyrażam zgodę na przetwarzanie przez SAME DEUTZ-FAHR Italia S.p.A. moich danych osobowych w innych celach marketingowych.
            </label>
            <span <?= ( $error['zgoda_handlowa2'] == true ) ? 'class="show"' : '' ; ?>>Zaznaczenie tego pola jest obowiązkowe</span>
        </div>

        <p class="small">* - pola obowiązkowe</p>


        <div class="form-submit-container">
            <input type="submit" value="Wyślij zgłoszenie" class="form_submit" data-id="<?= $i; ?>" data-disabled="true">
            <?php require( 'img/arrow.svg' ); ?>
        </div>

    </form>

    <?php
}


?>