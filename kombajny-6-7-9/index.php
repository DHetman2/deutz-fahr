<?php
ob_start();

session_start();
if ( isset($_GET['thank-you']) && $_SESSION['send'] != true ) {
    $_SESSION['send'] = false;
    header('location: http://promocje-deutz-fahr.pl/kombajny-6-7-9/');
}

$_SESSION['send'] = false;

?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <title>Deutz-Fahr Kombajny Serii 6, 7 i 9</title>
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css?v=2">
    <link rel="icon" type="image/x-icon" href="favicon.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700;800&display=swap" rel="stylesheet">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WB5VB9K');</script>
    <!-- End Google Tag Manager -->

</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WB5VB9K"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php if ( isset( $_GET['thank-you'] ) ) : ?>

    <section class="row thanks">

        <div class="l12 col text-center">
            <img src="img/deutz-fahr-logo.png" alt="Deutz Fahr">
        </div>

        <div class="l8 m10 s12 center col text-center">
            <p style="font-weight:bold">Dziękujemy za wypełnienie formularza.<br>
            <p>Niebawem zadzwonimy do Ciebie w celu przedstawienia indywidualnej oferty na&nbsp;<strong>kombajny Deutz-Fahr Serii 6, 7 i 9.</strong>.</p>
			<p>Do usłyszenia!<br>
			Zespół Deutz-Fahr</p>
        </div>

    </section>

    </body>
    </html>

    <?php
    die();

endif;

$validation = false;
$error = [];

if ( $_SERVER['REQUEST_METHOD'] == 'POST') :

    $data = filter_input_array(INPUT_POST, [
        "name" => FILTER_SANITIZE_STRING,
        "phone" => FILTER_SANITIZE_STRING,
        "powiat" => FILTER_SANITIZE_STRING,
        "email" => FILTER_SANITIZE_EMAIL    
    ]);

    $data["zgoda_dane"] = ( isset( $_POST['zgoda_dane'] ) ) ? 1 : 0;
    $data["zgoda_handlowa"] = ( isset( $_POST['zgoda_handlowa'] ) ) ? 1 : 0;
    $data["zgoda_handlowa2"] = ( isset( $_POST['zgoda_handlowa2'] ) ) ? 1 : 0;
    $date = date('Y-m-d H:i:s');

    if ( !empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif ( !empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    if ( trim($data['name']) == '' ) { $error["name"] = true; }
    if ( trim($data['phone']) == '' ) { $error["phone"] = true; }
    if ( trim($data['email']) == '' ) { $error["email"] = true; }
    if ( trim($data['powiat']) == '' ) { $error["powiat"] = true; }
    if ( $data['zgoda_dane'] != 1 ) { $error["zgoda_dane"] = true; }
    if ( $data['zgoda_handlowa'] != 1 ) { $error["zgoda_handlowa"] = true; }
   
    if ( count( $error ) == 0 ) {

        require('mail.php');

        $host = "213.77.69.224";
        $user = "promocjasdf";
        $pass = "6GBCu9iY6xHc";
        $db = "promocjasdf";
		$mysqli = new mysqli($host, $user, $pass, $db);
		mysqli_set_charset($mysqli, "utf8");

        $check = $mysqli->query("SELECT * FROM lp_kombajny_6_7_9 WHERE phone = '".$data['phone']."' ");
        $checkrow = $check->num_rows;

        if ( $checkrow == null ) {

            $result = $mysqli->query("INSERT INTO lp_kombajny_6_7_9 VALUES ('', '".$data['name']."', '".$data['phone']."', '".$data['email']."', '".$data['powiat']."', '".$data['zgoda_dane']."', '".$data['zgoda_handlowa']."', '".$data['zgoda_handlowa2']."', '".$date."', '".$ip."' ) ") or die( mysqli_error( $mysqli ) );

            sendConfirmationMail( $data['name'], $data['email'] );
            sendConfirmationMailToDF( $data['name'], $data['email'], $data['phone'], $data['powiat'], $data['zgoda_dane'], $data['zgoda_handlowa'], $data['zgoda_handlowa2'] );

        } else {

            $result = true;

        }

        if ( $result ) {

            $_SESSION['send'] = true;
            header ('location: ?thank-you');

        } else {

            echo '<p>Błąd, skontaktuj się z nami.</p>';

        }

    }

endif;

if ( $validation ==  false || count( $error ) > 0 ) :

?>

<main class="main">

    <header class="row-100" id="header">
        <div class="row">
            <div class="l6 m12 col">
                <span>
                    <img src="img/specjalna-przedsezonowa-oferta.png" srcset="img/specjalna-przedsezonowa-oferta.png x1, img/specjalna-przedsezonowa-oferta@2x.png x2" alt="Specjalna przedsezonowa oferta">
                </span>
                <h1>
                    <img src="img/h1-kombajn-deutz-fahr.png" srcset="img/h1-kombajn-deutz-fahr.png x1, img/h1-kombajn-deutz-fahr@2x.png x2" alt="Kombajn Deutz-Fahr coraz bliżej Ciebie">
                </h1>
                <a href="#formularz" class="button big" data-click="button">Poproś o ofertę <div class="arrow"></div></a>
            </div>
            <div class="l6 m12 col df-hero">
                <img src="img/deutz-fahr-c7000.png" alt="Deutz-Fahr C7000">
            </div>
        </div>
    </header>

    <section class="row-100 second">
        <div class="row">
            <div class="l12 col text-right">
                <img src="img/deutz-fahr-logo.png" alt="Deutz Fahr">
            </div>
        </div>
    </section>

    <section class="row third">
        <div class="l6 m12 col">
            <h2>Wybierz model</h2>
            <div class="subheader">Skorzystaj z promocji!</div>
            <p>Czeka na Ciebie:</p>
            <ul>
                <li>Przedsezonowa cena</li>
                <li>Pierwsza rata płatna za rok</li>
                <li>Gotowość na następny sezon</li>
            </ul>
            <a href="#formularz" class="button" data-click="button">Poproś o ofertę <div class="arrow"></div></a>
        </div>
        <div class="l6 m12 col text-center">
            <img src="img/deutz-fahr-c7000-studio.jpg" alt="Kombajn Deutz-Fahr C7000">
        </div>
    </section>

</main>

    <!-- FORMULARZ --> 

    <aside>

        <!-- <div class="hide-aside"><img src="img/arrow.svg" alt="Schowaj" class="arrow"></div> -->
        <section class="row">
            <div class="l12 center">
                <h2>Skorzystaj z&nbsp;promocji. Wypełnij formularz.</h2>
                <p>Zostaw nam dane kontaktowe w&nbsp;poniższym formularzu, a&nbsp;nasi doradcy skontaktują się z&nbsp;Tobą i&nbsp;zaproponują specjalną przedsezonową ofertę. Odpowiedzą również na&nbsp;Twoje pytania.</p>
                <form method="POST" action="./" id="form">
                <div class="field name">
                    <label>Imię i nazwisko*</label>
                    <input type="text" name="name" value="<?= ( isset($_POST['name']) ) ? htmlspecialchars($_POST['name']) : ''; ?>" required id="form-name">
                    <span <?= ( $error['name'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span>
                </div>
                <div class="field phone">
                    <label>Numer telefonu*</label>
                    <input type="text" name="phone" value="<?= ( isset($_POST['phone']) ) ? htmlspecialchars($_POST['phone']) : ''; ?>" required>
                    <span <?= ( $error['phone'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span>
                </div>
                <div class="field powiat">
                    <label>Powiat*</label>
                    <input type="text" name="powiat" value="<?= ( isset($_POST['powiat']) ) ? htmlspecialchars($_POST['powiat']) : ''; ?>" list="powiaty" required>
                    <span <?= ( $error['powiat'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span>
                    <?php require('powiaty.php'); ?>
                </div>
                <div class="field email">
                    <label>Adres e-mail*</label>
                    <input type="email" name="email" value="<?= ( isset($_POST['email']) ) ? htmlspecialchars($_POST['email']) : ''; ?>">     <span <?= ( $error['email'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span> 
                </div>

                <div class="field">
                    <p>Informacja o przetwarzaniu danych osobowych przez SAME DEUTZ&#8209;FAHR Italia S.p.A. – <a href="#przetwarzaniedanych" class="open-dane">czytaj więcej</a>.</p>
                    <div class="dane">
                        <p>Państwa dane osobowe  będą przetwarzane przez Administratora danych osobowych, tj. SAME DEUTZ&#8209;FAHR Italia S.p.A. z siedzibą przy Viale Francesco Cassani, 14 - 24047 Treviglio (BG) – Włochy na podstawie wyrażonej przez Państwa zgody (art. 6 ust. 1 lit. a RODO) i tylko w celu zarządzania Państwa prośbą dotyczącą kontaktu oraz w celu dostarczenia Państwu żądanych informacji. Podanie danych osobowych jest dobrowolne. Brak zgody uniemożliwi nam jednak realizację Państwa prośby. Dane będą przetwarzane przez Administratora wyłącznie przez czas konieczny do zrealizowania deklarowanego celu, tj. przez czas trwania kampanii marketingowej „NIE PRZEGAP OKAZJI! ŻNIWA Z NOWYM KOMBAJNEM” (o ile przepisy prawa nie stanowią inaczej). Państwa dane osobowe zostaną udostępnione partnerowi handlowemu Administratora (dealerowi marki DEUTZ&#8209;FAHR w Polsce), właściwemu ze względu na treść zgłoszonego przez Państwa żądania kontaktu. Informacja o dealerach dostępna jest pod linkiem: <a href="https://www.deutz-fahr.com/pl-pl/znajdz-dealera">https://www.deutz-fahr.com/pl-pl/znajdz-dealera</a>.</p>
                        <p>Państwa dane osobowe nie będą przetwarzane w celu zautomatyzowanego podejmowania decyzji, w tym poprzez profilowanie. 
                        W każdym czasie mogą Państwo wycofać zgodę na przetwarzanie Państwa danych osobowych. Wycofanie zgody nie wpływa na zgodność z prawem przetwarzania, którego dokonano na podstawie zgody przed jej wycofaniem.</p>
                        <p>Mają Państwo prawo do uzyskania potwierdzenia występowania bądź braku Państwa danych. Ponadto przysługuje Państwu prawo do uzyskania informacji na temat źródła i celu oraz sposobu przetwarzania danych, prawo do ich aktualizacji, sprostowania i uzupełnienia, a także do usunięcia danych przetwarzanych niezgodnie z prawem, co do przetwarzania których zgoda została wycofana lub jeśli dane te nie są już niezbędne do celów, dla których zostały zebrane. W celu skorzystania z przysługujących uprawnień mogą Państwo zwrócić się do administratora danych, kontaktując się z SAME DEUTZ&#8209;FAHR Italia S.p.A. z siedzibą przy Viale Francesco Cassani, 14 - 24047 Treviglio (BG) – Włochy bądź wysyłając wiadomość e-mail na adres: privacy@sdfgroup.com.
                        W razie uznania, że Państwa dane osobowe są przetwarzane niezgodnie z prawem, mają Państwo prawo do wystąpienia ze skargą do organu nadzorczego, tj. Prezesa Urzędu Ochrony Danych Osobowych w Polsce albo do organu wiodącego, tj. Garante per la protezione dei dati personali we Włoszech.</p>
                    </div>
                </div>

                <div class="field checkbox zgoda_dane">
                    <input type="checkbox" id="zgoda_dane" name="zgoda_dane" required<?= ( isset($_POST['zgoda_dane']) == true ) ? 'checked="checked"' : ''; ?>>
                    <label for="zgoda_dane">Wyrażam zgodę na przetwarzanie przez SAME DEUTZ&#8209;FAHR Italia S.p.A. i przez jej partnerów handlowych (dealerów marki DEUTZ-FAHR w Polsce) moich danych osobowych w celu przedstawiania mi informacji o interesujących mnie produktach, w tym informacji marketingowych*.</label>
                    <span <?= ( $error['zgoda_dane'] == true ) ? 'class="show"' : '' ; ?>>Zaznaczenie tego pola jest obowiązkowe</span>
                </div>

                <div class="field checkbox zgoda_handlowa">
                    <input type="checkbox" id="zgoda_handlowa" name="zgoda_handlowa" required <?= ( isset($_POST['zgoda_handlowa']) == true ) ? 'checked="checked"' : ''; ?>>
                    <label for="zgoda_handlowa">Wyrażam zgodę na kontakt telefoniczny lub e-mail przez SAME DEUTZ&#8209;FAHR Italia S.p.A. lub jej partnerów handlowych (dealerów marki DEUTZ-FAHR w Polsce) w celu przedstawiania mi informacji o interesujących mnie produktach, w tym informacji marketingowych*.</label>
                    <span <?= ( $error['zgoda_handlowa'] == true ) ? 'class="show"' : '' ; ?>>Zaznaczenie tego pola jest obowiązkowe</span>
                </div>

                <div class="field checkbox zgoda_handlowa2">
                    <input type="checkbox" id="zgoda_handlowa2" name="zgoda_handlowa2" <?= ( isset($_POST['zgoda_handlowa2']) == true ) ? 'checked="checked"' : ''; ?>>
                    <label for="zgoda_handlowa2">Wyrażam zgodę na przetwarzanie przez SAME DEUTZ&#8209;FAHR Italia S.p.A. moich danych osobowych w innych celach marketingowych.</label>
                </div>

                <div class="field">
                    <p class="small">* - pola obowiązkowe</p>
                </div>

                <div class="field">
                    <div id="form_submit" class="button">
                        <input type="submit" value="Wyślij zgłoszenie">
                        Wyślij zgłoszenie
                        <div class="arrow"></div>
                    </div>
                </div>

                </form>
            </div>
        </section>

    </aside>

    <!-- KONIEC FORMULARZA -->

<div class="main">

    <section class="row-100 green">
        <div class="row">
            <div class="l6 m12 col">
                <p>Więcej szczegółów uzyskasz u&nbsp;naszych doradców.<br><strong>Zostaw kontakt. Zadzwonimy.</strong></p>
            </div>
            <div class="l6 m12 col">
                <a href="#formularz" class="button big white" data-click="button">Poproś o ofertę <div class="arrow"></div></a>
            </div>
        </div>
    </section>

    <section class="row models">

        <div class="l4 m12 xs12 col">

            <img src="img/deutz-fahr-c6000-model@2x.jpg" alt="Kombajn Deutz-Fahr C6000">
            <h2>Deutz-Fahr C6000</h2>
            <h3>Twój kombajn, Twoje zbiory, Twój sukces</h3>
            <hr>
            <p>Chcesz być niezależny? Oczekujesz najlepszych parametrów i&nbsp;najwyższej jakości? Kombajny z&nbsp;serii <strong>DEUTZ&#8209;FAHR&nbsp;C6000</strong> to idealne rozwiązanie.</p>
            <ul>
                <li>Dedykowana dla małych i&nbsp;średnich gospodarstw</li>
                <li>Niezawodna i kompaktowa maszyna do zbioru każdego rodzaju plonu</li>
                <li>Silnik o pojemności 6,1&nbsp;l o&nbsp;mocy 250&nbsp;KM</li>
                <li>Hedery o szerokościach od 4,2 do 6,3&nbsp;m</li>
                <li>Segmentowe klepiska umożliwiające szybkie przezbrojenie</li>
                <li>Turboseparator zwiększający wydajność nawet o&nbsp;20%</li>
                <li>Dwa poziomy sit czyszczących</li>
                <li>Zbiornik ziarna 7000&nbsp;l</li>
                <li>Perfekcyjne przygotowanie słomy dzięki wydajnej sieczkarni</li>
                <li>Powrót niedomłotów DGR z&nbsp;minimłocarniami</li>
            </ul>

            <a href="#formularz" class="button white" data-click="button">Poproś o ofertę <div class="arrow"></div></a>

        </div>

        <div class="l4 m12 xs12 col">

            <img src="img/deutz-fahr-c7000-model@2x.jpg" alt="Kombajn Deutz-Fahr C7000">
            <h2>Deutz-Fahr C7000</h2>
            <h3>Moc i najwyższa jakość</h3>
            <hr>
            <p>Zastanawiasz się, jaki kombajn na 900&nbsp;ha lub 400&nbsp;ha kupić? Kombajny serii <strong>DEUTZ&#8209;FAHR&nbsp;C7000</strong> to optymalny wybór!</p>
            <ul>
                <li>Silnik o pojemności 7,7&nbsp;l o&nbsp;mocy od 313 do 353&nbsp;KM</li>
                <li>Hedery o szerokościach od 5,4&nbsp;m do 7,2&nbsp;m</li>
                <li>5- lub 6-klawiszowy system separacji</li>
                <li>Powierzchnia sit od 5,28 do 6,32&nbsp;m<sup>2</sup></li>
                <li>Zbiornik ziarna od 8500 do 9500&nbsp;l</li>
                <li>Możliwość programowania ustawień kombajnu w&nbsp;zależności od rodzaju rośliny</li>
                <li>Powrót niedomłotów DGR z&nbsp;minimłocarniami</li>
            </ul>

            <a href="#formularz" class="button white" data-click="button">Poproś o ofertę <div class="arrow"></div></a>

        </div>

        <div class="l4 m12 xs12 col">

            <img src="img/deutz-fahr-c9000-model@2x.jpg" alt="Kombajn Deutz-Fahr C9000">
            <h2>Deutz-Fahr C9000</h2>
            <h3>Nadzwyczajna wszechstronność</h3>
            <hr>
            <p>Posiadasz duży areał i&nbsp;poszukujesz ekonomicznego kombajnu? Cztery modele z&nbsp;<strong>Serii&nbsp;C9000</strong> to ekonomia eksploatacji, zredukowane koszty eksploatacji oraz prosta obsługa. Seria C9000 to nadzwyczajna wydajność ze&nbsp;szczególną wszechstronnością, eleganckim wzornictwem i&nbsp;wyjątkową funkcjonalnością.</p>
            <ul>
                <li>Seria DEUTZ-FAHR C9000 dedykowana dla dużych gospodarstw</li>
                <li>Ekonomiczna i wydajna propozycja dla najbardziej wymagających rolników</li>
                <li>Silnik 7,7&nbsp;l rozwija moc do 381&nbsp;KM</li>
                <li>Hedery o szerokościach od 4,2 do 9&nbsp;m</li>
                <li>System omłotu MaxiCrop</li>
                <li>Segmentowe klapiska i&nbsp;Turboseparator zwiększający wydajność nawet o&nbsp;20%</li>
                <li>Powierzchnia sit czyszczących od 5,28 do 6,32&nbsp;m<sup>2</sup></li>
                <li>Zbiornik ziarna 10&nbsp;500&nbsp;l</li>
                <li>Sieczkarnia wyposażona w&nbsp;nowe, sterowane elektronicznie kierownice</li>
                <li>Powrót niedomłotów DGR z&nbsp;minimłocarniami</li>
            </ul>

            <a href="#formularz" class="button white" data-click="button">Poproś o ofertę <div class="arrow"></div></a>

        </div>

    </section>

    <footer>
        <div class="row">
            <div class="l12 col text-right">
                <a href="#header" class="up">Wróć do góry</a>
                <ul>
                    <li><a class="instagram" href="https://www.instagram.com/deutzfahr_brand_official/" target="_blank">Instagram</a></li>
                    <li><a class="facebook" href="https://www.facebook.com/DeutzFahrPolska/" target="_blank">Facebook</a></li>
                    <li><a class="youtube" href="https://www.youtube.com/user/sdfpoland/videos?disable_polymer=1" target="_blank">YouTube</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="l6 xs12 col">
                <p>DEUTZ&#8209;FAHR JEST MARKĄ <img src="img/samedeutz-fahr.png" alt="SDF"></p>
            </div>
            <div class="l6 xs12 col">
                <a href="https://www.deutz-fahr.com/pl-pl/uwagi-prawne-i-ochrona-prywatnosci" target="_blank">Polityka prywatności</a>
            </div>
        </div>
    </footer>

</div>

<script>
function checkboxValidation( element ) {
    element.querySelector( 'input' ).addEventListener( 'change', function() {
        if ( element.querySelector( 'input' ).checked == true ) {
            element.querySelector( 'span' ).className = '';
        } else {
            element.querySelector( 'span' ).className = 'show';
        }
    })
}

function inputValidation( element ) {
    element.querySelector( 'input' ).addEventListener( 'keyup', function() {
        if ( element.querySelector( 'input' ).value != '' ) {

            if ( element.querySelector( 'input[name="phone"]' ) && validatePhone( element.querySelector('input[name="phone"]').value ) == false ) {
                element.querySelector( 'span' ).className = 'show';
            } else {
                element.querySelector( 'span' ).className = '';
            }
        } else {
            element.querySelector( 'span' ).className = 'show';
        }
    })
}

// function selectValidation( element ) {
//     element.querySelector( 'select' ).addEventListener( 'change', function() {
//         if ( element.querySelector( 'select' ).value != '' ) {
//             element.querySelector( 'span' ).className = '';
//         } else {
//             element.querySelector( 'span' ).className = 'show';
//         }
//     })
// }

function validateEmail( email ) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validatePhone( phone ) {
    var phoneno = /^([+]?\d{1,2}[.-\s]?)?(\d{3}[.-]?){2}\d{3}$/;
    return phoneno.test(String(phone).toLowerCase());
}

window.addEventListener('load', function() {

    document.querySelector('#form_submit').addEventListener( 'click', function( event ) {

        event.preventDefault();

        var error = false;
        var errors = [];

        if ( document.querySelector('#form input[name="name"]').value == '' ) {
            error = true;
            errors.push("name");
        }

        if ( document.querySelector('#form input[name="phone"]').value == '' || ( document.querySelector('#form input[name="phone"]').value != '' && validatePhone( document.querySelector('#form input[name="phone"]').value ) == false ) ) {
            error = true;
            errors.push('phone');
        }

        if ( document.querySelector('#form input[name="powiat"]').value == '' ) {
            error = true;
            errors.push("powiat");
        }

        if ( document.querySelector('#form input[name="email"]').value == '' ) {
            error = true;
            errors.push('email');
        }

        if ( document.querySelector('#form input[name="zgoda_dane"]').checked == false ) {
            error = true;
            errors.push('zgoda_dane');
        }

        if ( document.querySelector('#form input[name="zgoda_handlowa"]').checked == false ) {
            error = true;
            errors.push('zgoda_handlowa');
        }

        if ( error == false ) {

            document.querySelector('#form').submit();
            document.querySelector('#form #form_submit').classList.add('loading');

        } else {

            for ( var i = 0; i<errors.length; i++ ) {

                if ( document.querySelector( '#form .field.' + errors[i] + ' input[type="checkbox"]' ) ) {
                    var txt = 'Zaznaczenie tego pola jest obowiązkowe';
                    checkboxValidation( document.querySelector( '#form .field.' + errors[i] ) );
                } else {
                    var txt = 'Wypełnij poprawnie te pole';
                    inputValidation( document.querySelector( '#form .field.' + errors[i] ) );
                    console.log( errors[i] );
                }

                document.querySelector( '#form .field.' + errors[i] + ' span' ).innerHTML = txt;
                document.querySelector( '#form .field.' + errors[i] + ' span' ).className = 'show';

            }
            return false;

        }

    });

    document.querySelector(".open-dane").addEventListener( 'click', function( event ) {
        event.preventDefault();
        var dane = document.querySelector('.dane');
        if  (dane.style.display != 'block') {
            this.innerHTML = 'schowaj';
            dane.style.display = 'block';
        } else {
            this.innerHTML = 'czytaj dalej';
            dane.style.display = 'none';
        }
    });

    // Powiększanie formularza oraz focus na pierwsze pole do wypełnienia
    var buttons = document.querySelectorAll( '.button' );
    for ( var i = 0; i<buttons.length; i++ ) {
        buttons[i].addEventListener( 'click', function( event ) {
            // document.querySelector( 'aside' ).className = 'selected';
            document.getElementById( 'form-name' ).focus();
        });
    }

    // Odklikiwanie aside z formularzem
    // var main = document.querySelectorAll( '.main' );
    // for (var i = 0; i<main.length; i++ ) {
    //     main[i].addEventListener( 'click', function( event ) {
    //         if ( event.currentTarget ) {
    //             if ( event.target.getAttribute( 'data-click' ) != 'button' && event.target.parentNode.getAttribute( 'data-click' ) != 'button' ) {
    //                 document.querySelector( 'aside' ).className = '';
    //             }
    //         }
    //     });
    // }

    // Wysuwanie formularza
    // document.querySelector( 'aside' ).addEventListener( 'click', function( event ) {
    //     if ( this.className == '' ) {
    //         if ( event.currentTarget )  {
    //             document.querySelector( 'aside' ).className = 'selected';
    //         }
    //     } else {
    //         if ( event.currentTarget && event.target.className != 'hide-aside' && event.target.className != 'arrow' ) {
    //             document.querySelector( 'aside' ).className = 'selected';
    //         } else {
    //             document.querySelector( 'aside' ).className = '';
    //         }
    //     }
    // });

});
</script>

<?php

endif;

?>

<script src="js/cookie.js"></script>

</body>
</html>