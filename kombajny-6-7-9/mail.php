<?php

function sendConfirmationMail( $recipient_name, $recipient_mail ) {

  $subject = 'Deutz-Fahr Kombajny 6, 7 i 9 - potwierdzenie wypełnienia formularza';
  $message = '
  <html>
  <head>
    <title>Deutz-Fahr Kombajny 6, 7 i 9 - potwierdzenie wypełnienia formularza</title>
  </head>
  <body>
    <img src="https://promocje-deutz-fahr.pl/kombajny-6-7-9/img/deutz-fahr-logo.png" src="Deutz Fahr">
    <p style="font-weight:bold">Dziękujemy za zaufanie do marki DEUTZ-FAHR i wypełnienie formularza.<p>
    <p>Już wkrótce skontaktujemy się z Tobą w celu przedstawienia indywidualnej oferty na <strong>kombajny DEUTZ-FAHR Serii 6, 7 i 9</strong>.</p>
    <p>Do usłyszenia,<br>
    Zespół DEUTZ-FAHR</p>
  </body>
  </html>';

	require_once('class.phpmailer.php');    //dodanie klasy phpmailer
	require_once('class.smtp.php');    //dodanie klasy smtp
	$mail = new PHPMailer(true);    //utworzenie nowej klasy phpmailer
	$mail->isSMTP();
	$mail->CharSet = 'UTF-8';
	$mail->Host = 'localhost';
	$mail->SMTPAuth = true;
	$mail->Username = 'auto@promocje-deutz-fahr.pl';
	$mail->Password = 'HbxO3Dw1#dZ';
	$mail->setFrom('auto@promocje-deutz-fahr.pl','Deutz-Fahr');
	$mail->addAddress($recipient_mail);
	$mail->Subject = $subject;
	$mail->Body    = $message;
	$mail->IsHTML(true);
	$mail->send();

}

function sendConfirmationMailToDF( $recipient_name, $recipient_mail, $recipient_phone, $powiat, $zgoda_dane, $zgoda_handlowa, $zgoda_handlowa2) {

	$zgoda_dane = ( $zgoda_dane == 1 ) ? 'Tak' : 'Nie';
	$zgoda_handlowa = ( $zgoda_handlowa == 1 ) ? 'Tak' : 'Nie';
	$zgoda_handlowa2 = ( $zgoda_handlowa2 == 1 ) ? 'Tak' : 'Nie';

	$subject = 'Nowy lead - Deutz-Fahr Kombajny 6, 7 i 9';
	$message = '<html>
	<head>
	<title>Nowy lead - Deutz-Fahr Kombajny 6, 7 i 9</title>
	</head>
	<body>
	<p style="font-weight:bold">Nowy lead - Deutz-Fahr Kombajny 6, 7 i 9</p>
	<p>Nowa osoba wysłała swoje zgłoszenie poprzez formularz Deutz-Fahr Kombajny 6, 7 i 9:<p>
	<p>Imię i nazwisko: ' . $recipient_name . '<br>
	E-mail: ' . $recipient_mail . '<br>
	Telefon: ' . $recipient_phone . '<br>
	Powiat: ' . $powiat . '<br>
	Zgoda na przetwarzanie danych: ' . $zgoda_dane . '<br>
	Zgoda na kontakt: ' . $zgoda_handlowa . '<br>
	Zgoda na inne wykorzystanie: ' . $zgoda_handlowa2 . '<br>
	</p>
	<p>Miłego dnia,<br>
	Zespół Deutz-Fahr</p>
	</body>
	</html>';

	require_once('class.phpmailer.php');    //dodanie klasy phpmailer
    require_once('class.smtp.php');    //dodanie klasy smtp
    $mail = new PHPMailer(true);    //utworzenie nowej klasy phpmailer
	$mail->isSMTP();
	$mail->CharSet = 'UTF-8';
	$mail->Host = 'localhost';
	$mail->SMTPAuth = true;
	$mail->Username = 'auto@promocje-deutz-fahr.pl';
	$mail->Password = 'HbxO3Dw1#dZ';
	$mail->setFrom('auto@promocje-deutz-fahr.pl','Deutz-Fahr');
	$mail->addAddress('jaroslaw.figurski@sdfgroup.pl');
	$mail->addAddress('rafal.jarmul@sdfgroup.pl');
	$mail->Subject = $subject;
	$mail->Body    = $message;
	$mail->IsHTML(true);
	$mail->send();


}

?>