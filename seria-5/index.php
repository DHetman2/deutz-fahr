<?php
ob_start();

session_start();
if ( isset($_GET['thank-you']) && $_SESSION['send'] != true ) {
    $_SESSION['send'] = false;
    header('location: http://promocje-deutz-fahr.pl/seria-5/');
}

$_SESSION['send'] = false;

?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <title>Deutz-Fahr Seria 5</title>
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css?v=2">
    <link rel="icon" type="image/x-icon" href="favicon.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700;800&display=swap" rel="stylesheet">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WB5VB9K');</script>
    <!-- End Google Tag Manager -->

</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WB5VB9K"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php if ( isset( $_GET['thank-you'] ) ) : ?>

    <section class="row thanks">

        <div class="l12 col text-center">
            <img src="img/deutz-fahr-logo.png" alt="Deutz Fahr">
        </div>

        <div class="l8 m10 s12 center col text-center">
            <p style="font-weight:bold">Dziękujemy za wypełnienie formularza.<br>
            <p>Niebawem zadzwonimy do Ciebie w celu przedstawienia indywidualnej oferty na&nbsp;<strong>ciągniki Deutz-Fahr Serii 5</strong>.</p>
			<p>Do usłyszenia!<br>
			Zespół Deutz-Fahr</p>
        </div>

    </section>

    </body>
    </html>

    <?php
    die();

endif;

$validation = false;
$error = [];

if ( $_SERVER['REQUEST_METHOD'] == 'POST') :

    $data = filter_input_array(INPUT_POST, [
        "name" => FILTER_SANITIZE_STRING,
        "phone" => FILTER_SANITIZE_STRING,
        "powiat" => FILTER_SANITIZE_STRING,
        "email" => FILTER_SANITIZE_EMAIL    
    ]);

    $data["zgoda_dane"] = ( isset( $_POST['zgoda_dane'] ) ) ? 1 : 0;
    $data["zgoda_handlowa"] = ( isset( $_POST['zgoda_handlowa'] ) ) ? 1 : 0;
    $data["zgoda_handlowa2"] = ( isset( $_POST['zgoda_handlowa2'] ) ) ? 1 : 0;
    $date = date('Y-m-d H:i:s');

    if ( !empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif ( !empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    if ( trim($data['name']) == '' ) { $error["name"] = true; }
    if ( trim($data['phone']) == '' ) { $error["phone"] = true; }
    if ( trim($data['powiat']) == '' ) { $error["powiat"] = true; }
    if ( trim($data['email']) == '' ) { $error["email"] = true; }
    if ( $data['zgoda_dane'] != 1 ) { $error["zgoda_dane"] = true; }
    if ( $data['zgoda_handlowa'] != 1 ) { $error["zgoda_handlowa"] = true; }
   
    if ( count( $error ) == 0 ) {

        require('mail.php');

        $host = "213.77.69.224";
        $user = "promocjasdf";
        $pass = "6GBCu9iY6xHc";
        $db = "promocjasdf";
		$mysqli = new mysqli($host, $user, $pass, $db);
		mysqli_set_charset($mysqli, "utf8");


        $check = $mysqli->query("SELECT * FROM lp_seria_5 WHERE phone = '".$data['phone']."' ");
        $checkrow = $check->num_rows;

        if ( $checkrow == null ) {

            $result = $mysqli->query("INSERT INTO lp_seria_5 VALUES ('', '".$data['name']."', '".$data['phone']."', '".$data['email']."', '".$data['powiat']."', '".$data['zgoda_dane']."', '".$data['zgoda_handlowa']."', '".$data['zgoda_handlowa2']."', '".$date."', '".$ip."' ) ") or die( mysqli_error( $mysqli ) );

            sendConfirmationMail( $data['name'], $data['email'] );
            sendConfirmationMailToDF( $data['name'], $data['email'], $data['phone'], $data['powiat'], $data['zgoda_dane'], $data['zgoda_handlowa'], $data['zgoda_handlowa2'] );

        } else {

            $result = true;

        }

        if ( $result ) {

            $_SESSION['send'] = true;
            header ('location: ?thank-you');

        } else {

            echo '<p>Błąd, skontaktuj się z nami.</p>';

        }

    }

endif;

if ( $validation ==  false || count( $error ) > 0 ) :

?>

<main class="main">

    <header class="row-100 first" id="header">
        <picture>
            <source media="(max-width:640px)" srcset="img/5-powodow-baner-mobile.jpg">
            <img src="img/5-powodow-baner.jpg" alt="Pięć powodów">
        </picture>
    </header>

    <section class="row-100 second">
        <div class="row">
            <div class="l4 s12 col push-8 s-dont-push">
                <img src="img/deutz-fahr-logo.png" alt="Deutz Fahr">
            </div>
            <div class="l8 s12 col pull-4 s-dont-pull">
                <strong>Najczęściej wybierany</strong>
                <p>ciągnik rolniczy w Polsce<sup>*</sup> ma swojego następcę</p>
            </div>
        </div>
    </section>

    <section class="row third">
        <div class="l6 m12 col">
            <h1>Nowa seria&nbsp;5</h1>
            <div class="subheader">Od 95 do 126 KM</div>
            <p><strong>Dołącz do ćwierć tysiąca rolników w Polsce</strong>, którzy wybrali Deutz&#8209;Fahr&nbsp;5110G. Zamów indywidualną ofertę i&nbsp;sprawdź modele z&nbsp;nowej Serii 5.</p>
            <a href="#formularz" class="button" data-click="button">Zamów indywidualną ofertę <div class="arrow"></div></a>
        </div>
        <div class="l6 m12 col text-center relative">
            <img src="img/deutz-fahr-seria-5.jpg?v=2" alt="Deutz-Fahr Seria 5">
            <div class="atr-express">
                <p>1. miejsce w Polsce wg rankingu<sup>*</sup></p>
                <img src="img/atr-express-logo.png" alt="ATR Express">
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="l6 s12 col">
            <p class="przypis">* Ranking ATR Express, Najchętniej wybierane nowe ciągniki rolnicze w&nbsp;pierwszych siedmiu miesiącach 2021&nbsp;r.</p>
        </div>
    </section>

</main>

    <!-- FORMULARZ --> 

    <aside>

        <!-- <div class="hide-aside"><img src="img/arrow.svg" alt="Schowaj" class="arrow"></div> -->

        <section class="row">
            <div class="l12 center">
                <h2>Twoja indywidualna oferta</h2>
                <p>Zostaw nam dane kontaktowe w&nbsp;poniższym formularzu, a&nbsp;nasi doradcy skontaktują się z&nbsp;Tobą i&nbsp;zaproponują indywidualną ofertę. Odpowiedzą również na Twoje pytania.</p>
                <form method="POST" action="./" id="form">
                <div class="field name">
                    <label>Imię i nazwisko*</label>
                    <input type="text" name="name" value="<?= ( isset($_POST['name']) ) ? htmlspecialchars($_POST['name']) : ''; ?>" required id="form-name">
                    <span <?= ( $error['name'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span>
                </div>
                <div class="field phone">
                    <label>Numer telefonu*</label>
                    <input type="text" name="phone" value="<?= ( isset($_POST['phone']) ) ? htmlspecialchars($_POST['phone']) : ''; ?>" required>
                    <span <?= ( $error['phone'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span>
                </div>
                <div class="field powiat">
                    <label>Powiat*</label>
                    <input type="text" name="powiat" value="<?= ( isset($_POST['powiat']) ) ? htmlspecialchars($_POST['powiat']) : ''; ?>" list="powiaty" required>
                    <span <?= ( $error['powiat'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span>
                    <?php require('powiaty.php'); ?>
                </div>
                <div class="field email">
                    <label>Adres e-mail*</label>
                    <input type="email" name="email" value="<?= ( isset($_POST['email']) ) ? htmlspecialchars($_POST['email']) : ''; ?>">     <span <?= ( $error['email'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span>
                </div>

                <div class="field">
                    <p>Informacja o przetwarzaniu danych osobowych przez SAME DEUTZ&#8209;FAHR Italia S.p.A. – <a href="#przetwarzaniedanych" class="open-dane">czytaj więcej</a>.</p>
                    <div class="dane">
                        <p>Państwa dane osobowe  będą przetwarzane przez Administratora danych osobowych, tj. SAME DEUTZ&#8209;FAHR Italia S.p.A. z siedzibą przy Viale Francesco Cassani, 14 - 24047 Treviglio (BG) – Włochy na podstawie wyrażonej przez Państwa zgody (art. 6 ust. 1 lit. a RODO) i tylko w celu zarządzania Państwa prośbą dotyczącą kontaktu oraz w celu dostarczenia Państwu żądanych informacji. Podanie danych osobowych jest dobrowolne. Brak zgody uniemożliwi nam jednak realizację Państwa prośby. Dane będą przetwarzane przez Administratora wyłącznie przez czas konieczny do zrealizowania deklarowanego celu, tj. przez czas trwania kampanii marketingowej „NIE PRZEGAP OKAZJI! ŻNIWA Z NOWYM KOMBAJNEM” (o ile przepisy prawa nie stanowią inaczej). Państwa dane osobowe zostaną udostępnione partnerowi handlowemu Administratora (dealerowi marki DEUTZ&#8209;FAHR w Polsce), właściwemu ze względu na treść zgłoszonego przez Państwa żądania kontaktu. Informacja o dealerach dostępna jest pod linkiem: <a href="https://www.deutz-fahr.com/pl-pl/znajdz-dealera">https://www.deutz-fahr.com/pl-pl/znajdz-dealera</a>.</p>
                        <p>Państwa dane osobowe nie będą przetwarzane w celu zautomatyzowanego podejmowania decyzji, w tym poprzez profilowanie. 
                        W każdym czasie mogą Państwo wycofać zgodę na przetwarzanie Państwa danych osobowych. Wycofanie zgody nie wpływa na zgodność z prawem przetwarzania, którego dokonano na podstawie zgody przed jej wycofaniem.</p>
                        <p>Mają Państwo prawo do uzyskania potwierdzenia występowania bądź braku Państwa danych. Ponadto przysługuje Państwu prawo do uzyskania informacji na temat źródła i celu oraz sposobu przetwarzania danych, prawo do ich aktualizacji, sprostowania i uzupełnienia, a także do usunięcia danych przetwarzanych niezgodnie z prawem, co do przetwarzania których zgoda została wycofana lub jeśli dane te nie są już niezbędne do celów, dla których zostały zebrane. W celu skorzystania z przysługujących uprawnień mogą Państwo zwrócić się do administratora danych, kontaktując się z SAME DEUTZ&#8209;FAHR Italia S.p.A. z siedzibą przy Viale Francesco Cassani, 14 - 24047 Treviglio (BG) – Włochy bądź wysyłając wiadomość e-mail na adres: privacy@sdfgroup.com.
                        W razie uznania, że Państwa dane osobowe są przetwarzane niezgodnie z prawem, mają Państwo prawo do wystąpienia ze skargą do organu nadzorczego, tj. Prezesa Urzędu Ochrony Danych Osobowych w Polsce albo do organu wiodącego, tj. Garante per la protezione dei dati personali we Włoszech.</p>
                    </div>
                </div>

                <div class="field checkbox zgoda_dane">
                    <input type="checkbox" id="zgoda_dane" name="zgoda_dane" required<?= ( isset($_POST['zgoda_dane']) == true ) ? 'checked="checked"' : ''; ?>>
                    <label for="zgoda_dane">Wyrażam zgodę na przetwarzanie przez SAME DEUTZ&#8209;FAHR Italia S.p.A. i przez jej partnerów handlowych (dealerów marki DEUTZ-FAHR w Polsce) moich danych osobowych w celu przedstawiania mi informacji o interesujących mnie produktach, w tym informacji marketingowych*.</label>
                    <span <?= ( $error['zgoda_dane'] == true ) ? 'class="show"' : '' ; ?>>Zaznaczenie tego pola jest obowiązkowe</span>
                </div>

                <div class="field checkbox zgoda_handlowa">
                    <input type="checkbox" id="zgoda_handlowa" name="zgoda_handlowa" required <?= ( isset($_POST['zgoda_handlowa']) == true ) ? 'checked="checked"' : ''; ?>>
                    <label for="zgoda_handlowa">Wyrażam zgodę na kontakt telefoniczny lub e-mail przez SAME DEUTZ&#8209;FAHR Italia S.p.A. lub jej partnerów handlowych (dealerów marki DEUTZ-FAHR w Polsce) w celu przedstawiania mi informacji o interesujących mnie produktach, w tym informacji marketingowych*.</label>
                    <span <?= ( $error['zgoda_handlowa'] == true ) ? 'class="show"' : '' ; ?>>Zaznaczenie tego pola jest obowiązkowe</span>
                </div>

                <div class="field checkbox zgoda_handlowa2">
                    <input type="checkbox" id="zgoda_handlowa2" name="zgoda_handlowa2" <?= ( isset($_POST['zgoda_handlowa2']) == true ) ? 'checked="checked"' : ''; ?>>
                    <label for="zgoda_handlowa2">Wyrażam zgodę na przetwarzanie przez SAME DEUTZ&#8209;FAHR Italia S.p.A. moich danych osobowych w innych celach marketingowych.</label>
                </div>

                <div class="field">
                    <p class="small">* - pola obowiązkowe</p>
                </div>

                <div class="field">
                    <div id="form_submit" class="button">
                        <input type="submit" value="Wyślij zgłoszenie">
                        Wyślij zgłoszenie
                        <div class="arrow"></div>
                    </div>
                </div>

                </form>
            </div>
        </section>

    </aside>

    <!-- KONIEC FORMULARZA -->

<div class="main">

    <section class="row-100">
        <div class="l12">
            <img src="img/5-powodow-ikonki.jpg?v=2" alt="">
        </div>
    </section>

    <section class="row">
        <div class="l8 m12 s12 col">
            <h2>Najbardziej wszechstronny <span>w&nbsp;swojej klasie</span></h2>
        </div>
        <div class="clearfix"></div>
        <div class="l4 m12 col">
            <p>Seria 5 DEUTZ&#8209;FAHR to modułowe, konfigurowalne i&nbsp;elastyczne modele w&nbsp;segmencie średniej mocy, oferujące idealne rozwiązania dla potrzeb każdego użytkownika.</p>
            <p>Modele Serii 5&nbsp;DEUTZ&#8209;FAHR stanowią doskonałe rozwiązanie przy pracach polowych, uprawach rzędowych, sianokosach, pracy w&nbsp;zagrodzie oraz przy pracach wykonywanych na postoju. Nadzwyczajna wszechstronność tych ciągników jest efektem doskonałego wyposażenia.</p>
            <a href="#formularz" class="button white s-hide" data-click="button">Zamów indywidualną ofertę<div class="arrow"></div></a>
        </div>
        <div class="l4 m6 s12 col">
            <ul>
                <li>Moc maksymalna od 95 do 126&nbsp;KM</li>
                <li>Wydajny 3 i 4-cylindrowy silnik FARMotion</li>
                <li>3-stopniowa przekładnia Powershift z&nbsp;APS</li>
                <li>Nawet 4 prędkości WOM</li>
                <li>Wysoka wydajność pomp układu hydraulicznego</li>
                <li>Luksusowa 4-słupkowa kabina</li>
            </ul>
        </div>
        <div class="l4 m6 s12 col text-center silnik">
            <img src="img/farmotion-silnik.jpg" alt="Silnik FARMotion">
            <div><strong>Dedykowany do rolnictwa silnik FARMotion</strong></div>
        </div>

        <div class="l-hide s-show text-center">
            <a href="#formularz" class="button margin" data-click="button">Zamów indywidualną ofertę<div class="arrow"></div></a>
        </div>

    </section>

    <section class="row zdjecia">
        <div class="l4 xxs12 col">
            <img src="img/1.jpg" alt="Deutz-Farh Seria 5">
        </div>
        <div class="l4 xxs12 col">
            <img src="img/2.jpg" alt="Deutz-Farh Seria 5">
        </div>
        <div class="l4 xxs12 col">
            <img src="img/3.jpg" alt="Deutz-Farh Seria 5">
        </div>
    </section>

    <section class="row">
        <div class="l6 m12 col">
            <h2>Seria 5 Deutz&#8209;Fahr</h2>
        </div>
        <div class="l6 m-hide text-right">
            <a href="#formularz" class="button margin" data-click="button">Zamów indywidualną ofertę<div class="arrow"></div></a>
        </div>
        <div class="clearfix"></div>
        <div class="l3 m6 xs12 col">
            <h3>Doskonała zwrotność</h3>
            <p>Niezwykłą zwrotność uzyskano dzięki dużemu kątowi skrętu kół, jak też wielu innym innowacyjnym rozwiązaniom. Seria&nbsp;5 oferuje rewers PowerShuttle z&nbsp;możliwością regulacji, układ kierowniczy SDD, funkcję Stop&Go, skrzynie Powershift z&nbsp;automatyczną zmianą półbiegów APS.</p>
        </div>
        <div class="l3 m6 xs12 col">
            <h3>Wyjątkowy komfort</h3>
            <p>Doskonały poziom komfortu w&nbsp;Serii&nbsp;5 zapewnie nowa amortyzacja kabiny na hydraulicznych silent-blokach, pneumatyczna amortyzacja foteli, w&nbsp;pełni regulowana kolumna kierownicza, fotel pasażera, szyberdach, radio DAB+, dwa porty USB, uchwyt smartfona, klimatyzacja i&nbsp;nawet 16 świateł roboczych.</p>
        </div>
        <div class="l3 m6 xs12 col">
            <h3>Najwyższe w tej klasie bezpieczeństwo</h3>
            <p>Maksymalna prędkość jazdy osiągana przy niskich obrotach silnika, przekładnia Powershift HML z automatyczną zmianą półbiegów Powershift (APS), układ hamulcowy z&nbsp;tarczami hamulcowymi w&nbsp;kąpieli olejowej na wszystkich 4&nbsp;kołach, hydrauliczny hamulec postojowy (HPB) oraz automatyczne załączanie WOM.</p>
        </div>
        <div class="l3 m6 xs12 col">
            <h3>Niezwykle wszechstronny</h3>
            <p>Silnik FARMotion&nbsp;35 oraz FARMotion&nbsp;45, przekładnie oferujące nawet 60+60 przełożeń, wydajność układu hydraulicznego sięgająca 90&nbsp;l/min, WOM o&nbsp;4&nbsp;prędkościach, elektroniczny podnośnik tylny, elektrohydraulicznie sterowane funkcje, podnośniki i&nbsp;WOM z&nbsp;tyłu, jak i&nbsp;z&nbsp;przodu ciągnika, fabryczne przygotowanie do ładowacza czołowego.</p>
        </div>
    </section>

    <section class="row">
        <div class="l6 m12 col">
            <h2>Specyfikacja Serii&nbsp;5</h2>
        </div>
        <div class="l6 m-hide text-right">
            <a href="#formularz" class="button margin" data-click="button">Zamów indywidualną ofertę<div class="arrow"></div></a>
        </div>
        <div class="l12 col">
            <table>
                <thead>
                    <tr>
                        <td></td>
                        <td>5095</td>
                        <td>5105</td>
                        <td>5115</td>
                        <td>5125</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Silnik</td>
                        <td data-model="5095" class="xxs-hide">FARMotion 45</td>
                        <td data-model="5095, 5105, 5115, 5125"colspan="3">FARMotion 45</td>
                    </tr>
                    <tr>
                        <td>Norma emisji spalin</td>
                        <td data-model="Wszystkie modele" colspan="4">Stage V</td>
                    </tr>
                    <tr>
                        <td>Zasys powietrza</td>
                        <td data-model="Wszystkie modele" colspan="4">Turbosprężarka, Intercooler</td>
                    </tr>
                    <tr>
                        <td>Układ wtryskowy, ciśnienie maksymalne</td>
                        <td data-model="Wszystkie modele" colspan="4">Common Rail, 2000 bar</td>
                    </tr>
                    <tr>
                        <td>Moc maksymalna przy 2000 obr./min</td>
                        <td data-model="5095">70&nbsp;kW / 95&nbsp;KM</td>
                        <td data-model="5105">78&nbsp;kW / 106&nbsp;KM</td>
                        <td data-model="5115">85&nbsp;kW / 116&nbsp;KM</td>
                        <td data-model="5125">93&nbsp;kW / 126&nbsp;KM</td>
                    </tr>
                    <tr>
                        <td>Moc znamionowa przy 2200 obr./min</td>
                        <td data-model="5095">66,4&nbsp;kW / 90&nbsp;KM</td>
                        <td data-model="5105">74&nbsp;kW / 101&nbsp;KM</td>
                        <td data-model="5115">80,6&nbsp;kW / 110&nbsp;KM</td>
                        <td data-model="5125">88,2&nbsp;kW / 120&nbsp;KM</td>
                    </tr>
                    <tr>
                        <td>Masa</td>
                        <td data-model="5095">3950&nbsp;kg</td>
                        <td data-model="5105">3950&nbsp;kg</td>
                        <td data-model="5115">4250&nbsp;kg</td>
                        <td data-model="5125">4350&nbsp;kg</td>
                    </tr>
                    <tr>
                        <td>Elektroniczny podnośnik tylny</td>
                        <td data-model="5095"><div class="dot"></div></td>
                        <td data-model="5105"><div class="dot"></div></td>
                        <td data-model="5115"><div class="dot"></div></td>
                        <td data-model="5125"><div class="dot"></div></td>
                    </tr>

                </tbody>
            </table>
        </div>
    </section>

    <footer>
        <div class="row">
            <div class="l12 col text-right">
                <a href="#header" class="up">Wróć do góry</a>
                <ul>
                    <li><a class="instagram" href="https://www.instagram.com/deutzfahr_brand_official/" target="_blank">Instagram</a></li>
                    <li><a class="facebook" href="https://www.facebook.com/DeutzFahrPolska/" target="_blank">Facebook</a></li>
                    <li><a class="youtube" href="https://www.youtube.com/user/sdfpoland/videos?disable_polymer=1" target="_blank">YouTube</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="l6 xs12 col">
                <p>DEUTZ&#8209;FAHR JEST MARKĄ <img src="img/samedeutz-fahr.png" alt="SDF"></p>
            </div>
            <div class="l6 xs12 col">
                <a href="https://www.deutz-fahr.com/pl-pl/uwagi-prawne-i-ochrona-prywatnosci" target="_blank">Polityka prywatności</a>
            </div>
        </div>
    </footer>

</div>

<script>
function checkboxValidation( element ) {
    element.querySelector( 'input' ).addEventListener( 'change', function() {
        if ( element.querySelector( 'input' ).checked == true ) {
            element.querySelector( 'span' ).className = '';
        } else {
            element.querySelector( 'span' ).className = 'show';
        }
    })
}

function inputValidation( element ) {
    element.querySelector( 'input' ).addEventListener( 'keyup', function() {
        if ( element.querySelector( 'input' ).value != '' ) {

            if ( element.querySelector( 'input[name="phone"]' ) && validatePhone( element.querySelector('input[name="phone"]').value ) == false ) {
                element.querySelector( 'span' ).className = 'show';
            } else {
                element.querySelector( 'span' ).className = '';
            }
        } else {
            element.querySelector( 'span' ).className = 'show';
        }
    })
}

// function selectValidation( element ) {
//     element.querySelector( 'select' ).addEventListener( 'change', function() {
//         if ( element.querySelector( 'select' ).value != '' ) {
//             element.querySelector( 'span' ).className = '';
//         } else {
//             element.querySelector( 'span' ).className = 'show';
//         }
//     })
// }

function validateEmail( email ) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validatePhone( phone ) {
    var phoneno = /^([+]?\d{1,2}[.-\s]?)?(\d{3}[.-]?){2}\d{3}$/;
    return phoneno.test(String(phone).toLowerCase());
}

window.addEventListener('load', function() {

    document.querySelector('#form_submit').addEventListener( 'click', function( event ) {

        event.preventDefault();

        var error = false;
        var errors = [];

        if ( document.querySelector('#form input[name="name"]').value == '' ) {
            error = true;
            errors.push("name");
        }

        if ( document.querySelector('#form input[name="phone"]').value == '' || ( document.querySelector('#form input[name="phone"]').value != '' && validatePhone( document.querySelector('#form input[name="phone"]').value ) == false ) ) {
            error = true;
            errors.push('phone');
        }

        if ( document.querySelector('#form input[name="powiat"]').value == '' ) {
            error = true;
            errors.push("powiat");
        }

        if ( document.querySelector('#form input[name="email"]').value == '' ) {
            error = true;
            errors.push('email');
        }

        if ( document.querySelector('#form input[name="zgoda_dane"]').checked == false ) {
            error = true;
            errors.push('zgoda_dane');
        }

        if ( document.querySelector('#form input[name="zgoda_handlowa"]').checked == false ) {
            error = true;
            errors.push('zgoda_handlowa');
        }

        if ( error == false ) {

            document.querySelector('#form').submit();
            document.querySelector('#form #form_submit').classList.add('loading');

        } else {

            for ( var i = 0; i<errors.length; i++ ) {

                if ( document.querySelector( '#form .field.' + errors[i] + ' input[type="checkbox"]' ) ) {
                    var txt = 'Zaznaczenie tego pola jest obowiązkowe';
                    checkboxValidation( document.querySelector( '#form .field.' + errors[i] ) );
                } else {
                    var txt = 'Wypełnij poprawnie te pole';
                    inputValidation( document.querySelector( '#form .field.' + errors[i] ) );
                    console.log( errors[i] );
                }

                document.querySelector( '#form .field.' + errors[i] + ' span' ).innerHTML = txt;
                document.querySelector( '#form .field.' + errors[i] + ' span' ).className = 'show';

            }
            return false;

        }

    });

    document.querySelector(".open-dane").addEventListener( 'click', function( event ) {
        event.preventDefault();
        var dane = document.querySelector('.dane');
        if  (dane.style.display != 'block') {
            this.innerHTML = 'schowaj';
            dane.style.display = 'block';
        } else {
            this.innerHTML = 'czytaj dalej';
            dane.style.display = 'none';
        }
    });

    // Powiększanie formularza oraz focus na pierwsze pole do wypełnienia
    var buttons = document.querySelectorAll( '.button' );
    for ( var i = 0; i<buttons.length; i++ ) {
        buttons[i].addEventListener( 'click', function( event ) {
            // document.querySelector( 'aside' ).className = 'selected';
            document.getElementById( 'form-name' ).focus();
        });
    }

    // Odklikiwanie aside z formularzem
    // var main = document.querySelectorAll( '.main' );
    // for (var i = 0; i<main.length; i++ ) {
    //     main[i].addEventListener( 'click', function( event ) {
    //         if ( event.currentTarget ) {
    //             if ( event.target.getAttribute( 'data-click' ) != 'button' && event.target.parentNode.getAttribute( 'data-click' ) != 'button' ) {
    //                 document.querySelector( 'aside' ).className = '';
    //             }
    //         }
    //     });
    // }

    // Wysuwanie formularza
    // document.querySelector( 'aside' ).addEventListener( 'click', function( event ) {
    //     if ( this.className == '' ) {
    //         if ( event.currentTarget )  {
    //             document.querySelector( 'aside' ).className = 'selected';
    //         }
    //     } else {
    //         if ( event.currentTarget && event.target.className != 'hide-aside' && event.target.className != 'arrow' ) {
    //             document.querySelector( 'aside' ).className = 'selected';
    //         } else {
    //             document.querySelector( 'aside' ).className = '';
    //         }
    //     }
    // });

});
</script>

<?php

endif;

?>

<script src="js/cookie.js"></script>

</body>
</html>