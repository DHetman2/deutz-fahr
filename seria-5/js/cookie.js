function showCookieBar()
{
    if( document.cookie.indexOf( 'cookies_accepted=true' ) < 0 )
    {
        const aside = document.createElement( 'div' );
        const btn   = document.createElement( 'button' );

        btn.className = 'btn';
        btn.innerText = 'Rozumiem';
        btn.addEventListener( 'click', function( event ) {
            const date = new Date();
            date.setTime( date.getTime() + ( 30 * 24 * 60 * 60 * 1000 ) );
            document.cookie = 'cookies_accepted=true;expires=' + date.toUTCString() + ';path=/';
            document.body.removeChild( event.target.parentElement.parentElement );
        } );

        aside.id        = 'cookie-bar'; 
        aside.innerHTML = '<p><span>Ta strona korzysta z plików cookie, które pozwalają nam zrozumieć, w jaki sposób korzystasz z naszej strony. Jeśli chcesz dowiedzieć się więcej na temat działania plików cookie i naszych zasad w tym zakresie, <a href="https://www.deutz-fahr.com/pl-pl/uwagi-prawne-i-ochrona-prywatnosci" target="_blank">kliknij tutaj</a>.</span></p>';
        aside.children[ 0 ].appendChild( btn );

        document.body.appendChild( aside );
    }
}

window.addEventListener( 'load', function( event ) {
    showCookieBar();
} );