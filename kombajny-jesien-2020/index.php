<?php
ob_start();
ini_set('display_errors',0);
session_start();
if ( isset($_GET['thank-you']) && $_SESSION['send'] != true ) {
    $_SESSION['send'] = false;
    header('location: http://promocje-deutz-fahr.pl/');
}
$_SESSION['send'] = false;
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <title>DEUTZ-FAHR C7205 TS</title>
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" type="image/x-icon" href="favicon.png">
    <script src="js/cookie.js"></script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WB5VB9K');</script>
    <!-- End Google Tag Manager -->

</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WB5VB9K"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php if ( !isset( $_GET['thank-you'] ) ) : ?>
<main>
<?php endif; ?>

    <header class="row-100" id="logo">
        <div class="row">
        	<div class="l12 col">
        		<a href="https://www.deutz-fahr.com/pl-pl/">
        			<img src="img/deutz-fahr-logo.png" alt="Deutz-Fahr">
        		</a>
        	</div>
        </div>
    </header>

<?php if ( isset( $_GET['thank-you'] ) ) : ?>

    <section class="row first thanks">

        <div class="l10 m12 center col">
            <p style="font-weight:bold">Dziękujemy za wypełnienie formularza.<br>
            <p>Niebawem zadzwonimy do Ciebie w celu przedstawienia oferty na kombajny DEUTZ-FAHR.</p>
			<p>Do usłyszenia!<br>
			Zespół DEUTZ-FAHR</p>
        </div>

    </section>

    </body>
    </html>

    <?php
    die();

endif;

$validation = false;
$error = [];

if ( $_SERVER['REQUEST_METHOD'] == 'POST') :

    $data = filter_input_array(INPUT_POST, [
        "name" => FILTER_SANITIZE_STRING,
        "phone" => FILTER_SANITIZE_STRING,
        "email" => FILTER_SANITIZE_EMAIL    
    ]);

    $data["zgoda_dane"] = ( isset( $_POST['zgoda_dane'] ) ) ? 1 : 0;
    $data["zgoda_handlowa"] = ( isset( $_POST['zgoda_handlowa'] ) ) ? 1 : 0;
    $data["zgoda_handlowa2"] = ( isset( $_POST['zgoda_handlowa2'] ) ) ? 1 : 0;
    $date = date('Y-m-d H:i:s');

    if ( !empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif ( !empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    if ( trim($data['name']) == '' ) { $error["name"] = true; }
    if ( trim($data['phone']) == '' ) { $error["phone"] = true; }
    if ( trim($data['email']) == '' ) { $error["email"] = true; }
    if ( $data['zgoda_dane'] != 1 ) { $error["zgoda_dane"] = true; }
    if ( $data['zgoda_handlowa'] != 1 ) { $error["zgoda_handlowa"] = true; }
   
    if ( count( $error ) == 0 ) {

        require('mail.php');

        $host = "213.77.69.224";
		$user = "promocjasdf";
		$pass = "6GBCu9iY6xHc";
		$db = "promocjasdf";
		$mysqli = new mysqli($host, $user, $pass, $db);
		mysqli_set_charset($mysqli, "utf8");


        $check = $mysqli->query("SELECT * FROM lp_c7205ts WHERE phone = '".$data['phone']."' ");
        $checkrow = $check->num_rows;

        if ( $checkrow == null ) {

            $result = $mysqli->query("INSERT INTO lp_c7205ts VALUES ('', '".$data['name']."', '".$data['phone']."', '".$data['email']."', '".$data['zgoda_dane']."', '".$data['zgoda_handlowa']."', '".$data['zgoda_handlowa2']."', '".$date."', '".$ip."' ) ");

            sendConfirmationMail( $data['name'], $data['email'] );
            sendConfirmationMailToDF( $data['name'], $data['email'], $data['phone'], $data['zgoda_dane'], $data['zgoda_handlowa'], $data['zgoda_handlowa2'] );

        } else {

            $result = true;

        }
        $result = true;
        if ( $result ) {

            $_SESSION['send'] = true;
            header ('location: ?thank-you');

        } else {

            echo '<p>Błąd, skontaktuj się z nami.</p>';

        }

    }

endif;

if ( $validation ==  false || count( $error ) > 0 ) :

?>

    <section class="row-100 first">
        <div class="l12 col text-center relative">
<!--             <div class="row finansowanie">
                <div class="l12 col text-right">
                    <img src="img/deutz-fahr-naklejka.png" alt="Limitowana ilość">
                </div>
            </div> -->
        </div>
        <img src="img/deutz-fahr-1.jpg?v=1" alt="Deutz Fahr">
    </section>

    <section class="row second">
        <div class="l12 col bold serie">
            <h2>Teraz decydujesz, teraz zyskujesz!</h2>
            <p class="big">Specjalna oferta przedsezonowa 2020/2021 na kombajn DEUTZ-FAHR C7205 TS i nie tylko.</p>
            <p>Zamów teraz nowy kombajn DEUTZ-FAHR i zyskaj:</p>
            <ul class="bigger">
                <li>Promocyjne warunki cenowe</li>
                <li>Gwarancję ceny i terminowej dostawy</li>
                <li>Możliwość skorzystania z atrakcyjnych i różnorodnych programów finansowania fabrycznego</li>
            </ul>
            <p class="big">Zastanawiasz się, jaki kombajn na 100 ha lub 200 ha kupić?<br>
            DEUTZ-FAHR C7205 TS będzie idealnym wyborem!</p>

        </div>
    </section>

    <section class="row-100 green">
        <div class="row">
            <a href="#form" class="l10 s12 col center">
                <span>Wypełnij formularz<br>i&nbsp;poznaj wyjątkowo korzystne warunki zakupu</span>
                <div class="arrow"></div>
            </a>
        </div>
    </section>

<aside>

    <section class="row">
        <div class="l12 center">
            <h2><strong>Wypełnij formularz</strong> już teraz</h2>
            <form method="POST" action="" id="form">
            <div class="field name">
                <label>Imię i nazwisko*</label>
                <input type="text" name="name" value="<?= ( isset($_POST['name']) ) ? htmlspecialchars($_POST['name']) : ''; ?>" required>
                <span <?= ( $error['name'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span>
            </div>
            <div class="field phone">
                <label>Numer telefonu*</label>
                <input type="text" name="phone" value="<?= ( isset($_POST['phone']) ) ? htmlspecialchars($_POST['phone']) : ''; ?>" required>
                <span <?= ( $error['phone'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span>
            </div>
            <div class="field email">
                <label>Adres e-mail*</label>
                <input type="email" name="email" value="<?= ( isset($_POST['email']) ) ? htmlspecialchars($_POST['email']) : ''; ?>">     <span <?= ( $error['email'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span>          
            </div>

            <div class="field">
                <p>Informacja o przetwarzaniu danych osobowych przez SAME DEUTZ-FAHR Italia S.p.A. – <a href="#przetwarzaniedanych" class="open-dane">czytaj więcej</a>.</p>
                <div class="dane">
                    <p>Państwa dane osobowe  będą przetwarzane przez Administratora danych osobowych, tj. SAME DEUTZ-FAHR Italia S.p.A. z siedzibą przy Viale Francesco Cassani, 14 - 24047 Treviglio (BG) – Włochy na podstawie wyrażonej przez Państwa zgody (art. 6 ust. 1 lit. a RODO) i tylko w celu zarządzania Państwa prośbą dotyczącą kontaktu oraz w celu dostarczenia Państwu żądanych informacji. Podanie danych osobowych jest dobrowolne. Brak zgody uniemożliwi nam jednak realizację Państwa prośby. Dane będą przetwarzane przez Administratora wyłącznie przez czas konieczny do zrealizowania deklarowanego celu, tj. przez czas trwania kampanii marketingowej „NIE PRZEGAP OKAZJI! ŻNIWA Z NOWYM KOMBAJNEM” (o ile przepisy prawa nie stanowią inaczej). Państwa dane osobowe zostaną udostępnione partnerowi handlowemu Administratora (dealerowi marki DEUTZ-FAHR w Polsce), właściwemu ze względu na treść zgłoszonego przez Państwa żądania kontaktu. Informacja o dealerach dostępna jest pod linkiem: <a href="https://www.deutz-fahr.com/pl-pl/znajdz-dealera">https://www.deutz-fahr.com/pl-pl/znajdz-dealera</a>.</p>
                    <p>Państwa dane osobowe nie będą przetwarzane w celu zautomatyzowanego podejmowania decyzji, w tym poprzez profilowanie. 
                    W każdym czasie mogą Państwo wycofać zgodę na przetwarzanie Państwa danych osobowych. Wycofanie zgody nie wpływa na zgodność z prawem przetwarzania, którego dokonano na podstawie zgody przed jej wycofaniem.</p>
                    <p>Mają Państwo prawo do uzyskania potwierdzenia występowania bądź braku Państwa danych. Ponadto przysługuje Państwu prawo do uzyskania informacji na temat źródła i celu oraz sposobu przetwarzania danych, prawo do ich aktualizacji, sprostowania i uzupełnienia, a także do usunięcia danych przetwarzanych niezgodnie z prawem, co do przetwarzania których zgoda została wycofana lub jeśli dane te nie są już niezbędne do celów, dla których zostały zebrane. W celu skorzystania z przysługujących uprawnień mogą Państwo zwrócić się do administratora danych, kontaktując się z SAME DEUTZ-FAHR Italia S.p.A. z siedzibą przy Viale Francesco Cassani, 14 - 24047 Treviglio (BG) – Włochy bądź wysyłając wiadomość e-mail na adres: privacy@sdfgroup.com.
                    W razie uznania, że Państwa dane osobowe są przetwarzane niezgodnie z prawem, mają Państwo prawo do wystąpienia ze skargą do organu nadzorczego, tj. Prezesa Urzędu Ochrony Danych Osobowych w Polsce albo do organu wiodącego, tj. Garante per la protezione dei dati personali we Włoszech.</p>
                </div>
            </div>

            <div class="field checkbox zgoda_dane">
                <input type="checkbox" id="zgoda_dane" name="zgoda_dane" required<?= ( isset($_POST['zgoda_dane']) == true ) ? 'checked="checked"' : ''; ?>>
                <label for="zgoda_dane">Wyrażam zgodę na przetwarzanie przez SAME DEUTZ-FAHR Italia S.p.A. i przez jej partnerów handlowych (dealerów marki DEUTZ-FAHR w Polsce) moich danych osobowych w celu przedstawiania mi informacji o interesujących mnie produktach, w tym informacji marketingowych*.</label>
                <span <?= ( $error['zgoda_dane'] == true ) ? 'class="show"' : '' ; ?>>Zaznaczenie tego pola jest obowiązkowe</span>
            </div>

            <div class="field checkbox zgoda_handlowa">
                <input type="checkbox" id="zgoda_handlowa" name="zgoda_handlowa" required <?= ( isset($_POST['zgoda_handlowa']) == true ) ? 'checked="checked"' : ''; ?>>
                <label for="zgoda_handlowa">Wyrażam zgodę na kontakt telefoniczny lub e-mail przez SAME DEUTZ-FAHR Italia S.p.A. lub jej partnerów handlowych (dealerów marki DEUTZ-FAHR w Polsce) w celu przedstawiania mi informacji o interesujących mnie produktach, w tym informacji marketingowych*.</label>
                <span <?= ( $error['zgoda_handlowa'] == true ) ? 'class="show"' : '' ; ?>>Zaznaczenie tego pola jest obowiązkowe</span>
            </div>

            <div class="field checkbox zgoda_handlowa2">
                <input type="checkbox" id="zgoda_handlowa2" name="zgoda_handlowa2" <?= ( isset($_POST['zgoda_handlowa2']) == true ) ? 'checked="checked"' : ''; ?>>
                <label for="zgoda_handlowa2">Wyrażam zgodę na przetwarzanie przez SAME DEUTZ-FAHR Italia S.p.A. moich danych osobowych w innych celach marketingowych.</label>
            </div>

            <div class="field">
                <p class="small">* - pola obowiązkowe</p>
            </div>

            <div class="field text-center">
                <input type="submit" value="Wyślij zgłoszenie" id="form_submit">
            </div>

            </form>
        </div>
    </section>

</aside>

    <section class="row-100 info">

        <div class="row serie">

            <div class="l12 col text-center">


            <h2>DEUTZ-FAHR C7205&nbsp;TS</h2>
            
                <img src="img/deutz-fahr-2.jpg?v=1" alt="Deutz Fahr">

                <p class="big">Kombajn zbożowy DEUTZ-FAHR C7205 TS to maszyna o nowoczesnym designie, która wesprze Cię podczas żniw swoją mocą, wydajnością i nowoczesnymi rozwiązaniami technicznymi.</p>

                <ul>
                    <li>Silnik o pojemnośći 7,7 l i mocy 288 KM</li>
                    <li>Hedery o szerokościach 5,4 m – 7,2 m</li>
                    <li>5-klawiszowy system separacji</li>
                    <li>Powierzchnia sit 5,28 m<sup>2</sup></li>
                    <li>Zbiornik ziarna 8500 l</li>
                    <li>Możliwość programowania ustawień kombajnu<br>w&nbsp;zależności od&nbsp;rodzaju rośliny</li>
                    <li>Powrót niedomłotów DGR z minimłocarniami</li>
                </ul>

            </div>

        </div>

    </section>

    <section class="row-100">
        <img src="img/deutz-fahr-3.jpg?v=1" alt="Deutz Fahr">
    </section>

    <section class="row serie">

        <div class="l12 col text-center">
            
            <h2>SZYBKOŚĆ I EFEKTYWNOŚĆ</h2>

            <ul>
                <li>Bogaty wybór szerokości hederu od 5,4 do 7,2 m</li>
                <li>Duża sztywność ramy, zredukowane wibracje</li>
                <li>Planetarny napęd kosy</li>
                <li>Optymalny rozkład masy hedera</li>
                <li>Sterowanie funkcjami hedera za pomocą joysticka</li>
                <li>Nagarniacz sterowany hydraulicznie</li>
            </ul>

        </div>

    </section>

    <section class="row-100 green">
        <div class="row">
            <a href="#form" class="l10 s12 col center">
                <p>Szukasz najlepszych parametrów i efektywności w kombajnie zbożowym?</p>
                <span>Pozwól nam opowiedzieć,<br>co możesz osiągnąć z&nbsp;C7205&nbsp;TS</span>
                <div class="arrow"></div>
            </a>
        </div>
    </section>

    <section class="row-100">
        <img src="img/deutz-fahr-4.jpg?v=1" alt="Deutz Fahr">
    </section>

     <section class="row serie">

        <div class="l12 col text-center">
            
            <h2>Spektakularna wydajność</h2>

            <ul>
                <li>Największa powierzchnia sit 5,28 m<sup>2</sup> w tej klasie kombajnów</li>
                <li>Unikalny system powrotu niedomłotów DGR z minimłocarniami</li>
                <li>Duża powierzchnia separacji 7,36 m<sup>2</sup></li>
            </ul>

            <table class="gallery">
                <tr>
                    <td rowspan="2">
                        <img src="img/df-gallery-1.jpg?v=1" alt="Deutz-Fahr C7205 TS">
                    </td>
                    <td>
                        <img src="img/df-gallery-2.jpg?v=1" alt="Deutz-Fahr C7205 TS">
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="img/df-gallery-3.jpg?v=1" alt="Deutz-Fahr C7205 TS">
                    </td>
                </tr>
            </table>

            <h2>Komfort premium</h2>

            <ul>
                <li>Kabina COMMANDER CAB V</li>
                <li>Panoramiczna szyba przednia o widoczności do 180°</li>
                <li>Przyciemnione szyby atermiczne</li>
                <li>Pneumatycznie amortyzowany fotel operatora</li>
                <li>Amortyzacja kabiny na 4 Silent-Blockach</li>
                <li>Rozbudowany pakiet oświetlenia</li>
                <li>Klimatyzacja i ogrzewanie w standardzie</li>
                <li>Fotel pasażera ze składanym siedziskiem</li>
                <li>Podłokietnik ze zintegrowanym joystickiem</li>
            </ul>

        </div>

    </section>

    <section class="row-100 green">
        <div class="row">
            <a href="#form" class="l10 s12 col center">
                <p>Produkcja i sprzedaż kombajnów zbożowych o najwyższym komforcie to domena DEUTZ-FAHR.</p>
                <span>Zostaw nam kontakt do siebie,<br>a dowiesz się  więcej o&nbsp;wyposażeniu C7205&nbsp;TS</span>
                <div class="arrow"></div>
            </a>
        </div>
    </section>

    <section class="row-100">
        <img src="img/deutz-fahr-5.jpg?v=1" alt="Deutz Fahr">
    </section>

    <section class="row-100 green">
        <div class="row">
            <a href="#form" class="l10 s12 col center">
                <span>Sprzedaż kombajnów zbożowych 2020 DEUTZ-FAHR</span>
                <p>Zastanawiasz się, jaki kombajn kupić? Nie wiesz, gdzie kupić kombajn zbożowy C7205 TS? Pozwól nam wesprzeć Cię w najlepszym wyborze.</p>
                <div class="arrow"></div>
            </a>
        </div>
    </section>

    <section class="row video">

        <div class="l12 col text-center">
            <h2>Zobacz pracę kombajnu C7205 TS</h2>
        </div>

        <div class="l6 m12 col">
            <video width="800" controls>
                <source src="video/DF-C7205-TS-1.mp4" type="video/mp4">
                Twoja przeglądarka nie obsługuje video HTML5.
            </video>
        </div>
        <div class="l6 m12 col">
            <video width="800" controls>
                <source src="video/DF-C7205-TS-2.mp4" type="video/mp4">
                Twoja przeglądarka nie obsługuje video HTML5.
            </video>
        </div>
        <div class="l6 m12 col">
            <video width="800" controls>
                <source src="video/DF-C7205-TS-3.mp4" type="video/mp4">
                Twoja przeglądarka nie obsługuje video HTML5.
            </video>
        </div>
        <div class="l6 m12 col">
            <video width="800" controls>
                <source src="video/DF-C7205-TS-4.mp4" type="video/mp4">
                Twoja przeglądarka nie obsługuje video HTML5.
            </video>
        </div>

    </section>

    <footer>
        <div class="row">
            <div class="l12 col text-right">
                <a href="#logo" class="up">Wróć do góry</a>
                <ul>
                    <li><a class="instagram" href="https://www.instagram.com/deutzfahr_brand_official/" target="_blank">Instagram</a></li>
                    <li><a class="facebook" href="https://www.facebook.com/DeutzFahrPolska/" target="_blank">Facebook</a></li>
                    <li><a class="youtube" href="https://www.youtube.com/user/sdfpoland/videos?disable_polymer=1" target="_blank">YouTube</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="l6 xs12 col">
                <p>DEUTZ-FAHR JEST MARKĄ <img src="img/samedeutz-fahr.png" alt="SDF"></p>
            </div>
            <div class="l6 xs12 col">
                <a href="https://www.deutz-fahr.com/pl-pl/uwagi-prawne-i-ochrona-prywatnosci" target="_blank">Polityka prywatności</a>
            </div>
        </div>
    </footer>

</main>

<script>
function checkboxValidation( element ) {
    element.querySelector( 'input' ).addEventListener( 'change', function() {
        if ( element.querySelector( 'input' ).checked == true ) {
            element.querySelector( 'span' ).className = '';
        } else {
            element.querySelector( 'span' ).className = 'show';
        }
    })
}

function inputValidation( element ) {
    element.querySelector( 'input' ).addEventListener( 'keyup', function() {
        if ( element.querySelector( 'input' ).value != '' ) {

            if ( element.querySelector( 'input[name="phone"]' ) && validatePhone( element.querySelector('input[name="phone"]').value ) == false ) {
                element.querySelector( 'span' ).className = 'show';
            } else {
                element.querySelector( 'span' ).className = '';
            }
        } else {
            element.querySelector( 'span' ).className = 'show';
        }
    })
}

// function selectValidation( element ) {
//     element.querySelector( 'select' ).addEventListener( 'change', function() {
//         if ( element.querySelector( 'select' ).value != '' ) {
//             element.querySelector( 'span' ).className = '';
//         } else {
//             element.querySelector( 'span' ).className = 'show';
//         }
//     })
// }

function validateEmail( email ) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validatePhone( phone ) {
    var phoneno = /^([+]?\d{1,2}[.-\s]?)?(\d{3}[.-]?){2}\d{3}$/;
    return phoneno.test(String(phone).toLowerCase());
}

window.addEventListener('load', function() {

    document.querySelector('#form_submit').addEventListener( 'click', function( event ) {

        event.preventDefault();

        var error = false;
        var errors = [];

        if ( document.querySelector('#form input[name="name"]').value == '' ) {
            error = true;
            errors.push("name");
        }

        if ( document.querySelector('#form input[name="phone"]').value == '' || ( document.querySelector('#form input[name="phone"]').value != '' && validatePhone( document.querySelector('#form input[name="phone"]').value ) == false ) ) {
            error = true;
            errors.push('phone');
        }

        if ( document.querySelector('#form input[name="email"]').value == '' ) {
            error = true;
            errors.push('email');
        }

        if ( document.querySelector('#form input[name="zgoda_dane"]').checked == false ) {
            error = true;
            errors.push('zgoda_dane');
        }

        if ( document.querySelector('#form input[name="zgoda_handlowa"]').checked == false ) {
            error = true;
            errors.push('zgoda_handlowa');
        }

        if ( error == false ) {

            document.querySelector('#form').submit();

        } else {

            for ( var i = 0; i<errors.length; i++ ) {

                if ( document.querySelector( '#form .field.' + errors[i] + ' input[type="checkbox"]' ) ) {
                    var txt = 'Zaznaczenie tego pola jest obowiązkowe';
                    checkboxValidation( document.querySelector( '#form .field.' + errors[i] ) );
                } else {
                    var txt = 'Wypełnij poprawnie te pole';
                    inputValidation( document.querySelector( '#form .field.' + errors[i] ) );
                    console.log( errors[i] );
                }

                document.querySelector( '#form .field.' + errors[i] + ' span' ).innerHTML = txt;
                document.querySelector( '#form .field.' + errors[i] + ' span' ).className = 'show';

            }
            return false;

        }

    });

    document.querySelector(".open-dane").addEventListener( 'click', function( event ) {
        event.preventDefault();
        var dane = document.querySelector('.dane');
        if  (dane.style.display != 'block') {
            this.innerHTML = 'schowaj';
            dane.style.display = 'block';
        } else {
            this.innerHTML = 'czytaj dalej';
            dane.style.display = 'none';
        }
    })

});
</script>

<?php

endif;

?>

</body>
</html>