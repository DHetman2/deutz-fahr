<?php

function sendConfirmationMail( $recipient_name, $recipient_mail ) {

  $subject = 'Deutz-Fahr C7205 TS - potwierdzenie wypełnienia formularza';
  $message = '
  <html>
  <head>
    <title>Deutz-Fahr C7205 TS - potwierdzenie wypełnienia formularza</title>
  </head>
  <body>
    <img src="https://promocje-deutz-fahr.pl/img/deutz-fahr-logo.png" src="Deutz Fahr">
    <p style="font-weight:bold">Dziękujemy za zaufanie do marki DEUTZ-FAHR i wypełnienie formularza.<p>
    <p>Już wkrótce skontaktujemy się z Tobą w celu przedstawienia oferty na kombajn C7205 TS i pozostałe wyjątkowa maszyny z oferty DEUTZ-FAHR.</p>
    <p>Do usłyszenia,<br>
    Zespół DEUTZ-FAHR</p>
  </body>
  </html>';

	require_once('class.phpmailer.php');    //dodanie klasy phpmailer
	require_once('class.smtp.php');    //dodanie klasy smtp
	$mail = new PHPMailer(true);    //utworzenie nowej klasy phpmailer
	$mail->isSMTP();
	$mail->CharSet = 'UTF-8';
	$mail->Host = 'localhost';
	$mail->SMTPAuth = true;
	$mail->Username = 'auto@promocje-deutz-fahr.pl';
	$mail->Password = 'HbxO3Dw1#dZ';
	$mail->setFrom('auto@promocje-deutz-fahr.pl','Deutz-Fahr');
	$mail->addAddress($recipient_mail);
	$mail->Subject = $subject;
	$mail->Body    = $message;
	$mail->IsHTML(true);
	$mail->send();

}

function sendConfirmationMailToDF( $recipient_name, $recipient_mail, $recipient_phone, $zgoda_dane, $zgoda_handlowa, $zgoda_handlowa2) {

	$zgoda_dane = ( $zgoda_dane == 1 ) ? 'Tak' : 'Nie';
	$zgoda_handlowa = ( $zgoda_handlowa == 1 ) ? 'Tak' : 'Nie';
	$zgoda_handlowa2 = ( $zgoda_handlowa2 == 1 ) ? 'Tak' : 'Nie';

	$subject = 'Nowy lead - Deutz-Fahr C7205 TS';
	$message = '<html>
	<head>
	<title>Nowy lead - Deutz-Fahr C7205 TS</title>
	</head>
	<body>
	<p style="font-weight:bold">Nowy lead - Deutz-Fahr C7205 TS</p>
	<p>Nowa osoba wysłała swoje zgłoszenie poprzez formularz Deutz-Fahr C7205 TS:<p>
	<p>Imię i nazwisko: ' . $recipient_name . '<br>
	E-mail: ' . $recipient_mail . '<br>
	Telefon: ' . $recipient_phone . '<br>
	Zgoda na przetwarzanie danych: ' . $zgoda_dane . '<br>
	Zgoda na kontakt: ' . $zgoda_handlowa . '<br>
	Zgoda na inne wykorzystanie: ' . $zgoda_handlowa2 . '<br>
	</p>
	<p>Miłego dnia,<br>
	Zespół Deutz-Fahr</p>
	</body>
	</html>';

	require_once('class.phpmailer.php');    //dodanie klasy phpmailer
    require_once('class.smtp.php');    //dodanie klasy smtp
    $mail = new PHPMailer(true);    //utworzenie nowej klasy phpmailer
	$mail->isSMTP();
	$mail->CharSet = 'UTF-8';
	$mail->Host = 'localhost';
	$mail->SMTPAuth = true;
	$mail->Username = 'auto@promocje-deutz-fahr.pl';
	$mail->Password = 'HbxO3Dw1#dZ';
	$mail->setFrom('auto@promocje-deutz-fahr.pl','Deutz-Fahr');
	$mail->addAddress('rafal.jarmul@sdfgroup.pl');
	$mail->Subject = $subject;
	$mail->Body    = $message;
	$mail->IsHTML(true);
	$mail->send();


}

?>