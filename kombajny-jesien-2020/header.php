<header class="row">
	<div class="l12 col text-center">
		<a href="https://www.deutz-fahr.com/pl-pl/">
			<img style="float: left; padding: 20px 25px 0px 0px;" src="img/deutz-fahr-logo.png" alt="Deutz-Fahr">
		</a>
	</div>
</header>

<style>
header {padding-bottom:1rem;}
header img {max-width:100%;}
@media (max-width:640px) {
	header {text-align:center;}
	header a {width:100%; display:block;}
	header img {display:block; float:none !important; margin:0 auto !important; padding:10px !important;}
}
</style>