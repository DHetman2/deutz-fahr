function openButtons() {

    var questions = document.querySelectorAll('[data-target]');

    for ( var i = 0; i<questions.length; i++ ) {

        questions[i].addEventListener( 'click', function(event) {
            event.preventDefault();

            var target = this.getAttribute( 'data-target' );
            var targetElement = document.querySelector( '[data-name="' + target + '"]' );

            if ( targetElement.getAttribute( 'data-show' ) == 'true' ) {
                targetElement.setAttribute( 'data-show', 'false' );
                this.setAttribute( 'data-show', 'false' );
            } else {
                targetElement.setAttribute( 'data-show', 'true' );
                this.setAttribute( 'data-show', 'true' );
            }

        });

    }

}

function CTAscrollAndFocusForm() {

    var cta = document.querySelectorAll( '[href="#formularz"]' );
    for ( var i = 0; i<cta.length; i++ ) {

        cta[i].addEventListener( 'click', function( event ) {
            event.preventDefault();
            document.querySelector( '#formularz' ).scrollIntoView();
            document.querySelector( 'select[name="seria"]' ).focus();
        });

    }

}

function mobileMenu() {

    if ( document.querySelector( '.mobile-menu-button') ) {

        document.querySelector( '.mobile-menu-button').addEventListener( 'click', function() {

            document.querySelector( '.mobile-menu' ).setAttribute( 'data-show', 'true');

        });

        document.querySelector( '.mobile-menu-button-close').addEventListener( 'click', function() {

            document.querySelector( '.mobile-menu' ).setAttribute( 'data-show', 'false' );

        });

        // Wyłącznie menu po kliknięciu jakiegokolwiek linka w menu
        var links = document.querySelectorAll( '.mobile-menu a');

        for (var i = 0; i<links.length; i++ ) {

            links[i].addEventListener( 'click', function() {
                document.querySelector( '.mobile-menu' ).setAttribute( 'data-show', 'false' );
            });

        }


    }

}

window.addEventListener('load', function() {

    CTAscrollAndFocusForm();
    mobileMenu();
    openButtons();

    // Lightbox do galerii zdjęć
    GLightbox({
        selector: '.glightbox2'
    });

    // Slidery zdjęć
    if ( document.querySelector( '.splide-c6000' ) ) {

        var splide6 = new Splide( '.splide-c6000', {
            pagination: false
        } );
        splide6.mount({});

        var splide6ttv = new Splide( '.splide-c7000', {
            pagination: false
        } );
        splide6ttv.mount({});

        var splide7ttv = new Splide( '.splide-c9000', {
            pagination: false
        } );
        splide7ttv.mount({});

    }

    // Lazy loading
    var img = document.querySelectorAll( 'img[data-src]' );
    for ( var i = 0;  i<img.length; i++ ) {

        if ( img[i].getAttribute( 'data-srcset' ) ) {
            var srcset = img[i].getAttribute( 'data-srcset' );
            if ( srcset ) {
                img[i].setAttribute( 'srcset', srcset );
            }   
        }

        var src = img[i].getAttribute( 'data-src' );
        img[i].setAttribute( 'src', src );

    }

});