function checkboxValidation( element ) {
    element.querySelector( 'input' ).addEventListener( 'change', function() {
        if ( element.querySelector( 'input' ).checked == true ) {
            element.querySelector( 'span' ).className = '';
        } else {
            element.querySelector( 'span' ).className = 'show';
        }
    })
}

function selectValidation( element ) {
    element.querySelector( 'select' ).addEventListener( 'change', function() {
        if ( element.querySelector( 'select' ).value != '' ) {
            element.querySelector( 'span' ).className = '';
        } else {
            element.querySelector( 'span' ).className = 'show';
        }
    })
}

function inputValidation( element ) {
    element.querySelector( 'input' ).addEventListener( 'keyup', function() {
        if ( element.querySelector( 'input' ).value != '' ) {

            if ( element.querySelector( 'input[name="phone"]' ) && validatePhone( element.querySelector('input[name="phone"]').value ) == false ) {
                element.querySelector( 'span' ).className = 'show';
            } if ( element.querySelector( 'input[name="email"]' ) && validateEmail( element.querySelector('input[name="email"]').value ) == false ) {
                element.querySelector( 'span' ).className = 'show';
            } else {
                element.querySelector( 'span' ).className = '';
            }

        } else {
            element.querySelector( 'span' ).className = 'show';
        }
    })
}

function validateEmail( email ) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validatePhone( phone ) {
    phone = phone.replace(/\s/g, "");
    phone = phone.replace('-', "");
    var phoneno = /^([+]?\d{1,2}[.-\s]?)?(\d{3}[.-]?){2}\d{3}$/;
    return phoneno.test(String(phone).toLowerCase());
}

function checkInputsAndActiveSendButton() {

    var check = [];
    var inputsAll = document.querySelectorAll('.formularz input[type="text"], .formularz input[type="checkbox"]');
    var inputs = document.querySelectorAll('.formularz input[type="text"]');
    var checkboxes = document.querySelectorAll('.formularz input[type="checkbox"]');

    // Sprawdzenie wszystkich pól
    for ( var i = 0; i < inputsAll.length; i++ ) {
        check[ inputsAll[i].getAttribute('name') ] = false;
    }

    // Zdarzenia dla pól tekstowych
    for ( var i = 0; i < inputs.length; i++ ) {

        inputs[i].addEventListener( 'keyup', function( ) {

            for ( var i = 0; i < inputs.length; i++ ) {
                if ( inputs[i].value != '' ) {
                    check[ inputs[i].getAttribute('name') ] = true;
                } else {
                    check[ inputs[i].getAttribute('name') ] = false;
                }
            }

            checkInputs( check );
        });
    }

    // Zdarzenia dla checkboxów
    for ( var i = 0; i < checkboxes.length; i++ ) {

        checkboxes[i].addEventListener( 'change', function( ) {

            for ( var i = 0; i < checkboxes.length; i++ ) {
                if ( checkboxes[i].checked ) {
                    check[ checkboxes[i].getAttribute('name') ] = true;
                } else {
                    check[ checkboxes[i].getAttribute('name') ] = false;
                }
            }

            checkInputs( check );

        });
    }
}

function checkInputs( check ) {
    var activeSendButton = false;

    for ( var k in check ) {
        if ( check.hasOwnProperty(k) ) {
            if ( check[k] == true ) {
                activeSendButton = true;
            } else {
                activeSendButton = false;
                break;
            }
        }
    }

    if ( activeSendButton == true ) {
        document.querySelector('.formularz .form-submit-container').setAttribute( 'data-disabled', 'false' );
    } else {
        document.querySelector('.formularz .form-submit-container').setAttribute( 'data-disabled', 'true' );
    }
}


window.addEventListener('load', function() {

    checkInputsAndActiveSendButton();

    var submits = document.querySelectorAll(".form-submit-container");

    for ( var i = 0; i<submits.length; i ++) {
        submits[i].addEventListener("click", function(event) {

            var id = this.querySelector('.form_submit').getAttribute( 'data-id' );

            event.preventDefault();

            var error = false;
            var errors = [];

            if ( document.querySelector('.form[data-id="' + id + '"] select[name="seria"]').value == '' ) {
                error = true;
                errors.push("seria");
            }

            if ( document.querySelector('.form[data-id="' + id + '"] input[name="name"]').value == '' ) {
                error = true;
                errors.push("name");
            }

            if ( document.querySelector('.form[data-id="' + id + '"] input[name="email"]').value == '' || validateEmail( document.querySelector('.form[data-id="' + id + '"] input[name="email"]').value ) == false ) {
                error = true;
                errors.push("email");
            }

            if ( document.querySelector('.form[data-id="' + id + '"] input[name="powiat"]').value == '' ) {
                error = true;
                errors.push("powiat");
            }

            if ( document.querySelector('.form[data-id="' + id + '"] input[name="phone"]').value == '' || ( document.querySelector('.form[data-id="' + id + '"] input[name="phone"]').value != '' && validatePhone( document.querySelector('.form[data-id="' + id + '"] input[name="phone"]').value ) == false ) ) {
                error = true;
                errors.push("phone");
            } 

            if ( document.querySelector('.form[data-id="' + id + '"] input[name="zgoda_dane"]').checked == false ) {
                error = true;
                errors.push("zgoda_dane");
            }

            if ( document.querySelector('.form[data-id="' + id + '"] input[name="zgoda_handlowa1"]').checked == false ) {
                error = true;
                errors.push("zgoda_handlowa1");
            }

            if ( error == false ) {

                document.querySelector('.form[data-id="' + id + '"]').submit();

            } else {

                for ( var i = 0; i<errors.length; i++ ) {

                    if ( document.querySelector( '.form[data-id="' + id + '"] .field.' + errors[i] + ' input[type="checkbox"]' ) ) {
                        var txt = 'Zaznaczenie tego pola jest obowiązkowe';
                        checkboxValidation( document.querySelector( '.form[data-id="' + id + '"] .field.' + errors[i] ) );
                    } if ( document.querySelector( '.form[data-id="' + id + '"] .field.' + errors[i] + ' select' ) ) {
                        var txt = 'Zaznaczenie tego pola jest obowiązkowe';
                        selectValidation( document.querySelector( '.form[data-id="' + id + '"] .field.' + errors[i] ) );
                    } else {
                        var txt = 'Wypełnij poprawnie te pole';
                        inputValidation( document.querySelector( '.form[data-id="' + id + '"] .field.' + errors[i] ) );
                    }

                    document.querySelector( '.form[data-id="' + id + '"] .field.' + errors[i] + ' span' ).innerHTML = txt;
                    document.querySelector( '.form[data-id="' + id + '"] .field.' + errors[i] + ' span' ).className = 'show';

                    document.querySelector( '.field.name' ).scrollIntoView();

                }

                return false;

            }

        });
    }

    if ( document.querySelector( 'input[name="phone"]' ) ) {

		document.querySelector( 'input[name="phone"]' ).addEventListener( 'keypress', function(event) {

            var count = this.value;

            if ( count.length <= 10 ) {

                if ( count.length == 3 || count.length == 7 ) {
                    this.value = this.value + ' ';
                }

                var charCode = (event.which) ? event.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57) ) {
                    event.preventDefault();
                }
                return true;

            } else {
                event.preventDefault();
            }

        });
		
		document.querySelector( 'input[name="name"]' ).addEventListener( 'keypress', function(event) {

            var nameText = this.value;
			
            if ( nameText.indexOf( "www" ) >= 0 ) {
				alert( 'Nie możesz wpisać strony www!' );
            }
        });
    
    }

});