<?php

$base = 'https://promocje-deutz-fahr.pl/kombajny-6-7-9';
// $base = 'http://localhost.lo/Deutz-Fahr/kombajny-6-7-9-2022';
$version = 220801;

// Włącz plik form.php ze skryptami formularza
require( 'form.php' );

// Zabezpiecz przekierowaniem przed podwójną wysyłką formularza
redirect();

?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <title>Kombajny Deutz-Fahr - Twój kombajn, Twój czas, Twoje zbiory</title>
    <meta charset="utf-8">
    <meta name="description" content="S=Zapewnij sobie pełną niezależność w czasie żniw! Gotowe do pracy o każdej porze i w każdym terenie kombajny Deutz-Fahr serii 6, 7 i 9.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?= $base; ?>/css/grid.css?=<?= $version; ?>">
    <link rel="stylesheet" type="text/css" href="<?= $base; ?>/css/style.css?=<?= $version; ?>">
    <link rel="stylesheet" href="<?= $base; ?>/css/glightbox.min.css?=<?= $version; ?>">
    <link rel="stylesheet" href="<?= $base; ?>/css/splide.min.css?=<?= $version; ?>">
    <link rel="icon" href="<?= $base; ?>/img/favicon.png" type="image/png">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700;800&display=swap" rel="stylesheet">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WB5VB9K');</script>
    <!-- End Google Tag Manager -->

</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WB5VB9K"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Header – logo + menu -->

<div class="<?= ( !isset( $_GET['thank-you'] ) ) ? 'main' : ''; ?>">

    <header>
        <div class="row">
            <div class="l4 xs10 col" id="logo">
                <?php require( 'img/deutz-fahr-logo.svg' ); ?>
            </div>

            <?php
            if ( !isset( $_GET['thank-you'] ) ) :
            ?>

            <div class="l8 xs2 text-right col">
                <div class="mobile-menu-button l-hide m-hide s-hide xs-show" onclick="ga('send', 'event', 'Hamburger menu', 'click', 'Hamburger Menu' );">
                    <?php require( 'img/hamburger-menu.svg'); ?>
                </div>
                <div class="mobile-menu">
                    <a href="#formularz" class="header-cta">
                        <?php require( 'img/icon-kombajn.svg' ); ?>Zapytaj o kombajn
                    </a>
                    <ul class="s-hide xs-show">
                        <li><a href="#seria-c6000">C6000</a></li>
                        <li><a href="#seria-c7000">C7000</a></li>
                        <li><a href="#seria-c9000">C9000</a></li>
                    </ul>
                    <a href="#" class="mobile-menu-button-close l-hide m-hide s-hide xs-show">
                        Schowaj menu
                    </a>
                </div>
            </div>

            <?php
            endif;
            ?>

        </div>
    </header>

    <section class="hero">
        <div class="row">
            <picture>
                <img src="<?= $base; ?>/img/twoj-kombajn.jpg" srcset="<?= $base; ?>/img/twoj-kombajn@2x.jpg 2x" alt="Deutz-Fahr - Twój kombajn, Twój czas, Twoje zbiory">
            </picture>
        </div>
    </section>

</div>

<?php

thankYouPage();

if ( !isset( $_GET['thank-you'] ) ) :

?>

<div class="main">

    <!-- Nagłowek oraz tekst główny -->
    <section class="intro white">
        <div class="row">
            <div class="l12 col">
                <h1><strong class="green">Kombajny <nobr>Deutz-Fahr</nobr></strong></h1>
            </div>
            <div class="l6 s12 col">
                <p><strong>Zapewnij sobie pełną niezależność w&nbsp;czasie żniw!</strong> Gotowe do pracy o&nbsp;każdej porze i&nbsp;w&nbsp;każdym terenie kombajny Deutz-Fahr gwarantują, że&nbsp;samodzielnie realizujesz każde zadanie, nawet nieprzewidziane. Własny wydajny i&nbsp;niezawodny kombajn oszczędza Twój czas i&nbsp;zapewnia sukces Tobie i&nbsp;Twojemu gospodarstwu. Już czas poznać nadzwyczajne <strong>serie kombajnów C6000, C7000 oraz C9000!</strong></p>
                <a href="#formularz" class="button">Zapytaj o kombajn <span><?php require( 'img/arrow.svg' ); ?></span></a>
            </div>
            <div class="l6 s12 col">
                <img data-src="<?= $base; ?>/img/kombajn-deutz-fahr.jpg" data-srcset="<?= $base; ?>/img/kombajn-deutz-fahr@2x.jpg 2x" alt="Kombajn Deutz-Fahr C7000">
            </div>
        </div>
    </section>

</div>



    <!-- Formularz -->

    <section class="formularz" id="formularz">

        <div class="row">

            <div class="l12">

                <?php

                $validation = false;
                $error = [];

                saveData();

                if ( $validation == false || count( $error ) > 0 ) {

                    form(1);

                }

                ?>

            </div>

        </div>

    </section>

<div class="main">

    <!-- Seria C6000 -->
    <section class="seria" id="seria-c6000">
    
        <div class="row">

            <div class="l6 m12 col">

                <h2>Seria C6000</h2>
                <p class="lead">Idealna dla małych i średnich gospodarstw</p>
                <hr>
                <p>Kompaktowe i&nbsp;niezawodne maszyny dla rolników, którzy chcą być niezależni oraz efektywni w&nbsp;swojej pracy. Innowacyjne i ekonomiczne silniki kombajnów pozwalają zredukować koszty pracy, a&nbsp;innowacyjne rozwiązania techniczne zapewniają spełnienie wszystkich oczekiwań użytkownika w&nbsp;zakresie wydajności i&nbsp;jakości zbieranego plonu. Wszystko to gwarantuje Ci nieskrępowaną swobodę działania. <a href="#formularz">Już czas, żeby poznać go bliżej</a>.</p>

                <ul>
                    <li>Moc do <strong>230&nbsp;KM</strong></li>
                    <li>Pojemność <strong>61&nbsp;litra</strong></li>
                    <li>Szerokość hedera <strong>od 4,2 do 6,3&nbsp;m</strong></li>
                    <li>Zbiornik ziarna <strong>7000&nbsp;litrów</strong></li>
                </ul>

                <a href="#formularz" class="button seria-c6000">Zapytaj o kombajn <span><?php require( 'img/arrow.svg' ); ?></span></a>

            </div>

            <div class="l6 m12 col">

                <div class="splide splide-c6000" role="group">
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/dfc6000-1.jpg" alt="Kombajn Deutz-Fahr C6000">
                            </li>
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/dfc6000-2.jpg" alt="Kombajny Deutz-Fahr C6000">
                            </li>
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/dfc6000-3.jpg" salt="Kombajny Deutz-Fahr C6000">
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <!-- Seria C7000 -->
    <section class="seria white" id="seria-c7000">
    
        <div class="row">

            <div class="l6 m12 col">

                <h2>Seria C7000</h2>
                <p class="lead">Wysoka wydajność, duża moc</p>
                <hr>
                <p>Świetne połączenie doskonałych osiągów przy niskim zużyciu paliwa. Ponadto zaawansowana technologicznie konstrukcji pozwala na wykonywanie prac z&nbsp;wysoką wydajnością w&nbsp;trudnym terenie. Nawet w&nbsp;trudnych warunkach kombajny Deutz-Fahr serii C7000 zapewniają znakomitą wydajność i&nbsp;jakość młócenia oraz wysoki komfort pracy. Kombajny z&nbsp;tej serii zapewnią Ci całkowitą samodzielność podczas żniw. <a href="#formularz">Nie trać czasu, sprawdź</a>.</p>

                <ul>
                    <li>Moc do <strong>353&nbsp;KM</strong></li>
                    <li>Pojemność <strong>7,7&nbsp;litra</strong></li>
                    <li>Szerokość hedera <strong>od 4,2 do 9,0&nbsp;m</strong></li>
                    <li>Zbiornik ziarna <strong>do 9500&nbsp;litrów</strong></li>
                </ul>

                <a href="#formularz" class="button seria-c7000">Zapytaj o kombajn <span><?php require( 'img/arrow.svg' ); ?></span></a>

            </div>

            <div class="l6 m12 col">

                <div class="splide splide-c7000" role="group">
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/dfc7000-1.jpg" alt="Kombajn Deutz-Fahr C7000">
                            </li>
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/dfc7000-2.jpg" alt="Kombajn Deutz-Fahr C7000">
                            </li>
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/dfc7000-3.jpg" salt="Kombajn Deutz-Fahr C7000">
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <!-- Seria C9000 -->
    <section class="seria" id="seria-c9000">
    
        <div class="row">

            <div class="l6 m12 col">

                <h2>Seria C9000</h2>
                <p class="lead">Jakość i wydajność na każdym terenie</p>
                <hr>
                <p>Kombajny Deutz-Fahr serii C9000 zagwarantują Ci pełną niezależność. Posiadają najbardziej zaawansowane systemy sterowania i&nbsp;wysoką ergonomię, która zapewnia komfort pracy i&nbsp;najwyższa wydajność przy najlepszej jakości ziarna i&nbsp;słomy. Ponadto: duża pojemność i&nbsp;szybki wyładunek. Kombajny tej serii posiadają ogromny zbiornik ziarna, który pozwala na długi okres pracy bez konieczności wyładunku. <a href="#formularz">Czas to pieniądz, sprawdź szczegóły</a>.</p>

                <ul>
                    <li>Moc do <strong>381&nbsp;KM</strong></li>
                    <li>Pojemność <strong>7,7&nbsp;litra</strong></li>
                    <li>Szerokość hedera <strong>od 5,4 do 9,0&nbsp;m</strong></li>
                    <li>Zbiornik ziarna <strong>10&nbsp;500&nbsp;litrów</strong></li>
                </ul>

                <a href="#formularz" class="button seria-c9000">Zapytaj o kombajn <span><?php require( 'img/arrow.svg' ); ?></span></a>

            </div>

            <div class="l6 m12 col">

                <div class="splide splide-c9000" role="group">
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/dfc9000-1.jpg" alt="Kombajn Deutz-Fahr C9000">
                            </li>
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/dfc9000-2.jpg" alt="Kombajn Deutz-Fahr C9000">
                            </li>
                            <li class="splide__slide">
                                <img data-src="<?= $base; ?>/img/dfc9000-3.jpg" salt="Kombajn Deutz-Fahr C9000">
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <section class="info white">

        <div class="row">

            <div class="l12 col relative">

                <div class="warrior">

                    <div class="l6 m12 col">

                        <h2>100&nbsp;lat doświadczeń <nobr>Deutz-Fahr</nobr></h2>
                        <hr>
                        <p>Solidne, wydajne i&nbsp;komfortowe kombajny DEUTZ-FAHR to owoc stu lat doświadczeń oraz nieprzerwanego postępu w projektowaniu sprzętu żniwnego. Poświęciliśmy wiele czasu, osiągając perfekcję, po to, żebyś Ty mógł dziś oszczędzić swój czas, pracując efektywnie i&nbsp;szybko. Kombajny DEUTZ-FAHR świetnie spisują się w&nbsp;trudnych warunkach, zapewniają znakomitą wydajność i&nbsp;jakość młócenia w każdej sytuacji. Ich niepowtarzalny projekt Giugiaro Design wyróżnia Cię pośród innych rolników. Stylistyka jest wyrazem technologicznej dominacji marki DEUTZ-FAHR – gwarancji doskonałej wydajności zbioru oraz najwyższej jakości ziarna i&nbsp;słomy. To połączenie niemieckiej solidności i&nbsp;włoskiej awangardy. </p>

                        <a href="#formularz" class="button">Zapytaj o kombajn <span><?php require( 'img/arrow.svg' ); ?></span></a>

                    </div>

                    <div class="l6 m12 col">

                        <img data-src="<?= $base; ?>/img/deutz-fahr-doswiadczenie.png" data-srcset="<?= $base; ?>/img/deutz-fahr-doswiadczenie@2x.png 2x" alt="Kombajny Deutz-Fahr">

                    </div>

                </div>

            </div>

        </div>

    </section>

    <section class="boxy white">

        <div class="row">

            <div class="l12 col">
                <h3>Najlepsze i niezawodne rozwiązania dla Twojego gospodarstwa.<span>Made in germany.</span></h3>
            </div>

            <div class="frames">

                <div class="col">
                    <div>
                        <h4>Komfort pracy</h4>
                        <p>Kombajny Deutz-Fahr zapewnią Ci pracę w&nbsp;komfortowych warunkach. A twój komfort przełoży się na bezpieczeństwo i&nbsp;efektywność. Kabiny kombajnów Deutz-Fahr zapewniają niezakłóconą widoczność, przestrzeń i&nbsp;wygodę dla operatora. Są wyciszone, co przekłada się na redukcję zmęczenia, i&nbsp;posiadają klimatyzację, dzięki której wielogodzinna praca odbywa się w&nbsp;optymalnych warunkach.</p>
                    </div>
                </div>

                <div class="col">
                    <div>
                        <h4>Wydajne sterowanie</h4>
                        <p>W kombajnach Deutz-Fahr masz pełną kontrolę nad swoją maszyną. Sterowanie przekładnią, motowidłami, listwą, a&nbsp;także pozycjonowanie i&nbsp;uruchamianie rury wyładowczej ziarna - wszystkie te funkcje są zintegrowane w dźwigni Commander Stick. To ona, dzięki ergonomicznemu kształtowi, zapewnia precyzyjne, intuicyjne i&nbsp;w&nbsp;pełni bezpieczne sterowanie kombajnem, a&nbsp;komfort użytkowania należy do najwyższych w kategorii.</p>
                    </div>
                </div>

            </div>

            <div class="l12 col text-center">
                <a href="#formularz" class="button">Zapytaj o kombajn <span><?php require( 'img/arrow.svg' ); ?></span></a>
            </div>

        </div>

    </section>

    <!-- Galeria zdjęć -->
    <section id="gallery">

        <div class="row section-margin">
            <div class="l12 col text-center">
                <h3>Kombajny Deutz-fahr w akcji</h3>
            </div>
        </div>

        <div class="row">

            <div class="l3 m4 s6 col">
                <a href="<?= $base; ?>/img/df-1.jpg" class="glightbox2">
                    <img data-src="<?= $base; ?>/img/df-1-thumb.jpg">
                </a>
            </div>
            <div class="l3 m4 s6 col">
                <a href="<?= $base; ?>/img/df-2.jpg" class="glightbox2">
                    <img data-src="<?= $base; ?>/img/df-2-thumb.jpg">
                </a>
            </div>
            <div class="l3 m4 s6 col">
                <a href="<?= $base; ?>/img/df-3.jpg" class="glightbox2">
                    <img data-src="<?= $base; ?>/img/df-3-thumb.jpg">
                </a>
            </div>
            <div class="l3 m4 s6 col">
                <a href="<?= $base; ?>/img/df-4.jpg" class="glightbox2">
                    <img data-src="<?= $base; ?>/img/df-4-thumb.jpg">
                </a>
            </div>

            <div class="l3 m4 s6 col">
                <a href="<?= $base; ?>/img/df-5.jpg" class="glightbox2">
                    <img data-src="<?= $base; ?>/img/df-5-thumb.jpg">
                </a>
            </div>
            <div class="l3 m4 s6 col">
                <a href="<?= $base; ?>/img/df-6.jpg" class="glightbox2">
                    <img data-src="<?= $base; ?>/img/df-6-thumb.jpg">
                </a>
            </div>
            <div class="l3 m4 s6 col">
                <a href="<?= $base; ?>/img/df-7.jpg" class="glightbox2">
                    <img data-src="<?= $base; ?>/img/df-7-thumb.jpg">
                </a>
            </div>
            <div class="l3 m4 s6 col">
                <a href="<?= $base; ?>/img/df-8.jpg" class="glightbox2">
                    <img data-src="<?= $base; ?>/img/df-8-thumb.jpg">
                </a>
            </div>

        </div>

        <div class="row">
            <div class="l12 col text-center">
                <a href="#formularz" class="button">Zapytaj o kombajn <span><?php require( 'img/arrow.svg' ); ?></span></a>
            </div>
        </div>

    </section>

</div>

    <?php

endif;

?>

<a href="#formularz" class="mobile-cta" onclick="ga('send', 'event', 'Przyklejone menu mobilne', 'click', 'Przyklejone menu mobilne');">
    <div class="row">
        <div class="l6 col">
            <p>Kombajny Deutz-Fahr</p>
        </div>
        <div class="l6 col">
            <?php require( 'img/arrow.svg' ); ?><br>
            Zapytaj o kombajn
        </div>
    </div>
</a>

<div class="<?= ( !isset( $_GET['thank-you'] ) ) ? 'main' : ''; ?>">

    <footer>
        <div class="row">
            <div class="l12 col text-right">
                <a href="#logo" class="up">Wróć do góry</a>
                <ul>
                    <li><a class="instagram" href="https://www.instagram.com/deutzfahr_brand_official/" target="_blank">Instagram</a></li>
                    <li><a class="facebook" href="https://www.facebook.com/DeutzFahrPolska/" target="_blank">Facebook</a></li>
                    <li><a class="youtube" href="https://www.youtube.com/user/sdfpoland/videos?disable_polymer=1" target="_blank">YouTube</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="l6 xs12 col">
                <p>DEUTZ&#8209;FAHR JEST MARKĄ <img data-src="<?= $base; ?>/img/samedeutz-fahr.png" alt="SDF"></p>
            </div>
            <div class="l6 xs12 col">
                <a href="https://www.deutz-fahr.com/pl-pl/uwagi-prawne-i-ochrona-prywatnosci" target="_blank">Polityka prywatności</a>
            </div>
        </div>
    </footer>

</div>

<script src="<?= $base; ?>/js/cookie.js?=<?= $version; ?>"></script>
<script src="<?= $base; ?>/js/glightbox.min.js?=<?= $version; ?>"></script>
<script src="<?= $base; ?>/js/general.js?=<?= $version; ?>"></script>
<script src="<?= $base; ?>/js/form.js?=<?= $version; ?>"></script>
<script src="<?= $base; ?>/js/splide.min.js?=<?= $version; ?>"></script>

</body>
</html>
